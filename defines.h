#ifndef _DEFINES_H
#define _DEFINES_H


#define MODE_NONE               0
#define MODE_EDIT_TRIANGLE      1
#define MODE_EDIT_MATERIAL      2
#define MODE_NEW_TRIANGLE       3
#define MODE_NEW_MATERIAL       4

#define EDIT_MODE_NONE          0
#define EDIT_MODE_SELECT        1
#define EDIT_MODE_MOVE          2
#define EDIT_MODE_SCALE         3

#define SELECT_MODE_GROUP       1
#define SELECT_MODE_TRIANGLE    2
#define SELECT_MODE_VERTEX      3

#define TIME_LINE_PRECISION	100.0

#define TIMER_INTERVAL		11

#endif // DEFINES_H
