#include "allinc.h"

Editor::Editor(int &argc, char **argv) : QApplication(argc, argv)
{

}

Editor::~Editor(void)
{

}

void Editor::Init(const char *file)
{
	filename = 0;
	animation_mode = false;
	busy = false;

	preview.axes = true;
	preview.both_sides = false;
	preview.normals = false;
	preview.wireframe = false;

	mesh = new CMesh();
	mesh->SetRefreshFunc(RefreshApp);
	selection = new Selection(this);

	main_win = new MainWindow(this);

	setlocale(LC_NUMERIC,"C");

	main_win->show();

	if(file)
		Load(file);
}

bool Editor::Load(const char *file)
{
	bool r;

	SetFileName(file);
	r = mesh->LoadFromFile(file);

	animation_mode = false;
	main_win->RefreshAnimation();
	main_win->RefreshTitle();
	main_win->RefreshLists();
	main_win->Refresh2DPreview();

	return r;
}


void Editor::RefreshUi(void)
{
	if(main_win != 0)
		main_win->RefreshUi();
}


bool Editor::LoadFromFile_libobj(const char *file, int merge)
{
	if(merge)
		return MergeWithFile_libobj(file, mesh);
	else
		return LoadMeshFromFile_libobj(file, mesh);
}

void Editor::CalculateNormalsSolid(void)
{
	set<CTriangle *>::iterator i;
	int c, m;

	SetBusy(true);
	SetStatusMessage(true, "Calculating solid Normals...");
	SetProgress(true);
	RefreshApp();

	selection->ConvertToTriangles();

	m = selection->selected_t.size();

	for(i=selection->selected_t.begin(), c=0; i!=selection->selected_t.end(); i++, c++)
	{
		(*i)->CalculateNormalSolid();
		SetProgress((float)c / (float)m);
		RefreshApp();
	}

	SetBusy(false);
	RefreshApp();
}

void Editor::CalculateNormalsSmooth(void)
{
	set<CTriangle *>::iterator i;
	set<CVertex *>::iterator v;
	int c, m, vi;

	SetBusy(true);
	SetStatusMessage(true, "Calculating smooth Normals...");
	SetProgress(true);
	RefreshApp();

	selection->ConvertToTriangles();
	m = selection->selected_t.size();

	for(i=selection->selected_t.begin(), c=0; i!=selection->selected_t.end(); i++, c++)
	{
		(*i)->CalculateNormalSolid();   // Calculate solid Normals of Triangles
		(*i)->v[0]->normal = (*i)->v[1]->normal = (*i)->v[2]->normal = Vec(0.0, 0.0, 0.0);  // Reset Vertex Normals
		(*i)->v[0]->normal_count = (*i)->v[1]->normal_count = (*i)->v[2]->normal_count = 0;
		SetProgress(((float)c / (float)m) * 0.33333);
		RefreshApp();
	}

	selection->ConvertToVertices();
	m = selection->selected_v.size();

	for(v=selection->selected_v.begin(), c=0; v!=selection->selected_v.end(); v++, c++)
	{
		for(i=selection->selected_t.begin(), c=0; i!=selection->selected_t.end(); i++, c++)
		{
			for(vi=0; vi<3; vi++)
			{
				if(*((*i)->v[vi]) == **v)
				{
					(*v)->normal += (*i)->fnormal; // Add Triangle Normals to Vertices
					(*v)->normal_count++;
				}
			}
		}

		SetProgress(((float)c / (float)m) * 0.33333 + 0.33333);
		RefreshApp();
	}


	for(v=selection->selected_v.begin(), c=0; v!=selection->selected_v.end(); v++, c++)
	{
		if((*v)->normal_count <= 1)
			continue;

		(*v)->normal *= 1.0 / (float)(*v)->normal_count; // Calculate the average
		(*v)->normal_count = 1;

		SetProgress(((float)c / (float)m) * 0.33333 + 0.66667);
		RefreshApp();
	}

	SetBusy(false);
	RefreshApp();
}

QString GetEntityAttributeTypeString(CEntityAttribute::Type t)
{
	switch(t)
	{
		case CEntityAttribute::VECTOR:		return QString("3D Vec");
		case CEntityAttribute::VECTOR2:		return QString("2D Vec");
		case CEntityAttribute::FLOAT:		return QString("Float");
		case CEntityAttribute::INT:			return QString("Int");
		case CEntityAttribute::STRING:		return QString("String");
		default:							return QString();
	}
}
