#include "allinc.h"

GLWin::GLWin(Editor *editor, QWidget *parent) : QGLWidget(parent)
{
	this->editor = editor;

	cam_dist = 5.0;
	cam_rot.Set(M_PI / 2.0, 0.0);
	focus.Set(0.0, 0.0, 0.0);

	mouse_pressed = 0;

	preview = 0;
	preview_t = 0;

	SetSelection(editor->GetSelection());

	view_angle = 60.0;

	x_tex = y_tex = z_tex = 0;
}

GLWin::~GLWin()
{
}

void GLWin::InitializeGL()
{
	CEngine::Init();
}

void GLWin::CalculateCamPos(void)
{
	if(cam_rot.y > M_PI / 2.0 - 0.0001)
		cam_rot.y = M_PI / 2.0  - 0.0001;

	if(cam_rot.y < -M_PI / 2.0  + 0.0001)
		cam_rot.y = -M_PI / 2.0  + 0.0001;

	if(cam_dist < 0.0)
		cam_dist = 0.05;

	cam_pos = Vec(cos(cam_rot.x) * cos(cam_rot.y) * cam_dist,
	sin(cam_rot.y) * cam_dist,
	sin(cam_rot.x) * cos(cam_rot.y) * cam_dist) + focus;

}

void GLWin::PaintAxes(void)
{
	CVector normal;
	glPointSize(4.0);

	/*if(!x_tex)
	x_tex = LoadGLTexture("./images/x.tga");
	if(!y_tex)
	y_tex = LoadGLTexture("./images/y.tga");
	if(!z_tex)
	z_tex = LoadGLTexture("./images/z.tga");*/


	UseNoShader();

	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	normal = cam_pos - Vec(0.0, 0.0, 0.0);
	normal.Normalize();
	normal.NormalToGL();

	SetColor(GL_DIFFUSE, 1.0, 1.0, 0.0);
	glBegin(GL_LINES);
	glVertex3f(0.0, -1.0, 0.0);
	glVertex3f(0.0, 1.0, 0.0);
	glEnd();

	SetColor(GL_DIFFUSE, 1.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex3f(-1.0, 0.0, 0.0);
	glVertex3f(1.0, 0.0, 0.0);
	glEnd();

	SetColor(GL_DIFFUSE, 0.0, 0.0, 1.0);
	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, -1.0);
	glVertex3f(0.0, 0.0, 1.0);
	glEnd();

	/*glActiveTextureARB(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, x_tex);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);*/

	glPointSize(5.0);

	SetColor(GL_DIFFUSE, 1.0, 0.0, 0.0);
	glBegin(GL_POINTS);
	glVertex3f(1.1, 0.0, 0.0);
	glEnd();

	SetColor(GL_DIFFUSE, 0.0, 1.0, 0.0);
	glBindTexture(GL_TEXTURE_2D, y_tex);
	glBegin(GL_POINTS);
	glVertex3f(0.0, 1.1, 0.0);
	glEnd();

	SetColor(GL_DIFFUSE, 0.0, 0.0, 1.0);
	glBindTexture(GL_TEXTURE_2D, z_tex);
	glBegin(GL_POINTS);
	glVertex3f(0.0, 0.0, 1.1);
	glEnd();

	//glDisable(GL_BLEND);
	//glDisable(GL_TEXTURE_2D);
}

void GLWin::PaintNormals(void)
{
	int i;
	CVertex *v;

	UseNoShader();
	glDisable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	glColor4f(1.0, 1.0, 1.0, 1.0);
	glBegin(GL_LINES);
	for(i=0; i<editor->GetMesh()->GetVertexCount(); i++)
	{
		v = editor->GetMesh()->GetVertex(i);
		glColor4f(1.0, 1.0, 1.0, 1.0);
		v->PutToGL();
		glColor4f(1.0, 0.0, 0.0, 1.0);
		((CVector)(*v) + v->normal).PutToGL();
	}
	glEnd();
}

void GLWin::PaintGL()
{
	CVector normal;
	set<CTriangle *>::iterator t;
	set<CVertex *>::iterator v;

	qglClearColor(QColor(1.0, 1.0, 1.0));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //Buffer clearen

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(view_angle, (double)width / (double)height, 0.05, 10000.0); //Winkel, Seitenverhältnis, nearclip, farclip

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	gluLookAt(cam_pos.x, cam_pos.y, cam_pos.z, focus.x, focus.y, focus.z, 0.0, 1.0, 0.0); //pos x, y, z, to x, y, z

	glEnable(GL_DEPTH_TEST);

	CEngine::BindFaceShader();
	CVector light_dir = cam_pos - focus;
	light_dir.Normalize();
	CEngine::GetFaceShader()->SetLight(light_dir * 100.0, Vec(0.7, 0.7, 0.7));
	//face_shader.SetTwoSide(0);
	CEngine::GetFaceShader()->SetBorder(0, Vec(0.0, 0.0), Vec(0.0, 0.0));

	editor->GetMesh()->SetWireframe((int)editor->preview.wireframe);

	editor->GetMesh()->TriggerAllVBOsRefresh();
	editor->GetMesh()->TriggerIBOsRefresh();
	editor->GetMesh()->PutToGL(cam_pos, (int)editor->preview.both_sides);

	if(preview && preview_t)
		preview_t->PutToGL(1);

	if(editor->preview.axes)
		PaintAxes();

	if(editor->preview.normals)
		PaintNormals();

	glDisable(GL_DEPTH_TEST);

	UseNoShader();

	normal = cam_pos - Vec(0.0, 0.0, 0.0);
	normal.Normalize();

	switch(public_selection->select_mode)
	{
		case SELECT_MODE_TRIANGLE:
		case SELECT_MODE_GROUP:
			for(t=editor->GetSelection()->selected_t.begin(); t!=editor->GetSelection()->selected_t.end(); t++)
				(*t)->PutToGL(1);
			break;
		case SELECT_MODE_VERTEX:
			SetColor(GL_DIFFUSE, 1.0, 0.0, 0.0);
			glPointSize(5.0);
			glBegin(GL_POINTS);
			normal.NormalToGL();
			for(v=editor->GetSelection()->selected_v.begin(); v!=editor->GetSelection()->selected_v.end(); v++)
				(*v)->PutToGL();
			glEnd();
		break;
	}
}

#define MIDDLE_X width / 2
#define MIDDLE_Y height / 2

 void GLWin::mousePressEvent(QMouseEvent *event)
 {
	 QGLWidget::mousePressEvent(event);

	 if(event->buttons() & Qt::LeftButton)
	 {
		 mouse_pressed = 1;

		 old_mouse_x = event->globalX();
		 old_mouse_y = event->globalY();

		 QCursor::setPos(mapToGlobal(QPoint(MIDDLE_X, MIDDLE_Y)));
		 setCursor(QCursor(Qt::BlankCursor));

		 if(event->modifiers() & Qt::ShiftModifier)
		 {
			 focus_move_x = Cross(cam_pos - focus, Vec(0.0, 1.0, 0.0));
			 focus_move_x.Normalize();
			 focus_move_y = Cross(cam_pos - focus, focus_move_x);
			 focus_move_y.Normalize();

			 mouse_pressed = 2;
		 }
	 }
 }


 void GLWin::mouseReleaseEvent(QMouseEvent *event)
 {
	 QGLWidget::mouseReleaseEvent(event);
	 if(!mouse_pressed)
		 return;
	 mouse_pressed = 0;
	 QCursor::setPos(old_mouse_x, old_mouse_y);
	 setCursor(QCursor(Qt::ArrowCursor));
 }

 void GLWin::mouseMoveEvent(QMouseEvent *event)
 {
	 if(!mouse_pressed)
		 return;

	 if(mouse_pressed == 1)
	 {
		 cam_rot.y += (   event->y() - (height / 2.0)   ) * 0.005;
		 cam_rot.x += (   event->x() - (width / 2.0)    ) * 0.005;
	 }
	 else if(mouse_pressed == 2)
	 {
		 focus -= focus_move_y * (   event->y() - (height / 2.0)   ) * 0.005;
		 focus += focus_move_x * (   event->x() - (width / 2.0)    ) * 0.005;
	 }
	 QCursor::setPos(mapToGlobal(QPoint(MIDDLE_X, MIDDLE_Y)));
 }

 void GLWin::wheelEvent(QWheelEvent *event)
 {
	 float speed;

	 if(event->modifiers() & Qt::ShiftModifier)
		speed = 0.2;
	 else if(event->modifiers() & Qt::ControlModifier)
		speed = 1.3;
	 else
		speed = 0.5;

	 cam_dist += speed * -((float)event->delta() / 120.0);
 }

void GLWin::resizeEvent(QResizeEvent *event)
 {
	 QGLWidget::resizeEvent(event);
	 ResizeGL(event->size().width(), event->size().height());
 }

 void GLWin::ResizeGL(int width, int height)
 {
	 glViewport(0, 0, width, height);

	 glMatrixMode(GL_PROJECTION);
	 glLoadIdentity();
	 glOrtho(-0.5, +0.5, +0.5, -0.5, 4.0, 15.0);
	 glMatrixMode(GL_MODELVIEW);

	 this->width = width;
	 this->height = height;
 }
