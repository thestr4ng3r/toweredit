
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "allinc.h"

#include <QtGui>
#include "ui_mainwindow.h"

class MainWindow : public QMainWindow, private Ui::MainWinUi
{
	Q_OBJECT

public:
	int real_edit_mode;

	MainWindow(Editor *editor, QWidget *parent = 0);
	~MainWindow();

	void InitGL(void);

	void RefreshLists(void);

	void RefreshUi(void);

public slots:
	void AboutQt(void);
	void AboutTowerEdit(void);

	void Open(void);
	void Save(void);
	void SaveAs(void);
	void ApplyT(void);
	void ApplyM(void);

	void MergeLib3DS(void) { ImportLib3DS(1); }
	void ImportLib3DS(int merge = 0);
	void MergeLibG3D(void) { ImportLibG3D(1); }
	void ImportLibG3D(int merge = 0);
	void MergeLibOBJ(void) { ImportLibOBJ(1); }
	void ImportLibOBJ(int merge = 0);

	void DeleteTriangle(void);

	void RefreshPreviewSettings(void);
	void RefreshPreviewSettingsCheck(void);

	void EditTButtonC(bool checked);
	void NewTButtonC(bool checked);
	void CloneTButtonC(void);
	void EditMButtonC(bool checked);
	void NewMButtonC(bool checked);

	void SelectButtonC(void);
	void MoveButtonC(void);
	void ScaleButtonC(void);

	void SelectionChange(void);
	void FlipSelectedT(void);
	void SeperateTriangles(void);
	void CreateFlippedTriangles(void);
	void CleanUpMesh(void);
	void SimplifyMesh(void);

	void SetPreviewRedraw(void);

	void CloneT(CTriangle *t);
	void CloneM(CMaterial *m);
	void ResetT(void);
	void ResetM(void);

	void SetMaterial(void);

	void ChangeSelectModeC(void);

	void OpenTextureMapper(void);
	void OpenMatrixDialog(void);
	void OpenEntityAttributeEditor(void);
	void RenameEntity(void);
	void SetEntityGroup(void);
	void NewEntity(void);
	void NewEntityAttribute(void);
	void DeleteEntity(void);
	void DeleteEntityAttribute(void);

	void CalculateNormals(void);

	void RefreshWidgetActions(void); // Dock Widgets
	void RefreshWidgets(void);

	void RefreshPositionList(void); // Positions
	void ChangePosition(const QString text);
	void ChangePosition(CMeshPosition *p);
	void DeletePosition(void);
	void NewPosition(void);

	void RefreshAnimation(float time = 0.0); // Animation
	void RefreshAnimationList(void);
	void RefreshAnimationWidget(void);
	void RefreshAnimationTime(int time);
	void RefreshAnimationTime(void);
	void RefreshEntityList(void);
	void RefreshEntityAttributeList(void);
	void RefreshMode(void);
	void RefreshTitle(void);
	void Refresh2DPreview(void);
	void Refresh2DPreviewMode(void);
	void NewAnimation(void);
	void DeleteAnimation(void);
	void NewKeyFrame(void);
	void DeleteKeyFrame(void);
	void JumpToKeyFrame(int r);
	void JumpToKeyFrame(void) { JumpToKeyFrame(KeyFrameList->currentRow()); }

	void ShowMeshInfo(void);

	void TEdit(void) { RefreshMode(); }

private:
	Editor *editor;

	GLWin *gl_win;

	int tri_nr;
	CTriangle **tri_list;
	int tex_nr;
	CMaterial **tex_list;
	CTriangle *edit_triangle;
	CMaterial *edit_material;

	int apply_selection;
	int selected_m_nr;
	CMaterial **selected_m;
	int mode;
	int edit_mode, k_edit_mode;

	Preview2D *preview2d[3];

	int animation_mode;
	int edit_key_mode;
	CAnimation *edit_anim;
	CKeyFrame **key_list;
	CKeyFrame *edit_key;

	TextureMapper *texture_mapper;
	MatrixDialog *matrix_dialog;
	NormalsCalculator *normals_calculator;
	EntityAttributeEditor *entity_attribute_editor;

	QProgressBar *status_progress_bar;

	void PreviewTriangle(void);
	void SelectList(void);
	void SyncDockAction(QDockWidget *d, QAction *a, int dir); // 0 means d -> a; 1 means the opposite

protected:
	void closeEvent(QCloseEvent *event);
	void timerEvent(QTimerEvent *event);

	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H
