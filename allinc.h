#ifndef _ALLINC_H
#define _ALLINC_H

#include <set>

#include "defines.h"
#include "towerengine.h"

#include "obj.hpp"

class Editor;
class MainWindow;
class GLWin;
class Preview2D;
class MatrixDialog;
class TextureMapper;
class NormalsCalculator;
class EntityAttributeEditor;
class TMapPreview;

#include "objloader.h"
#include "image.h"
#include "view.h"
#include "selection.h"

#include "editor.h"
#include "glwin.h"
#include "mainwindow.h"
#include "preview2d.h"
#include "texturemapper.h"
#include "matrixdialog.h"
#include "texturemapper.h"
#include "normalscalculator.h"
#include "tmappreview.h"
#include "entityattributeeditor.h"

#endif // ALLINC_H
