#include "allinc.h"

CMesh *loading_mesh;
vector<CVector2> uv_coords;
vector<CVertex *> vertices;

void GeometryVertexCallback(obj::float_type x, obj::float_type y, obj::float_type z);
void TextureVertexCallack(obj::float_type x, obj::float_type y);
void TriangularFaceVerticesCallback(obj::index_type a, obj::index_type b, obj::index_type c);
void TriangularFaceVerticesTextureVerticesCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c);
void TriangularFaceVerticesNormalsCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c);
void TriangularFaceVerticesTextureVerticesNormalsCallback(const obj::index_3_tuple_type &a, const obj::index_3_tuple_type &b, const obj::index_3_tuple_type &c);
void QuadrilateralFaceVerticesCallback(obj::index_type a, obj::index_type b, obj::index_type c, obj::index_type d);
void QuadrilateralFaceVerticesTextureVerticesCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c, const obj::index_2_tuple_type &d);
void QuadrilateralFaceVerticesNormalsCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c, const obj::index_2_tuple_type &d);
void QuadrilateralFaceVerticesTextureVerticesNormalsCallback(const obj::index_3_tuple_type &a, const obj::index_3_tuple_type &b, const obj::index_3_tuple_type &c, const obj::index_3_tuple_type &d);
void PolygonalFaceVerticesBeginCallback(obj::index_type a, obj::index_type b, obj::index_type c);
void PolygonalFaceVerticesVertexCallback(obj::index_type a);
void PolygonalFaceVerticesEndCallback(void);
void PolygonalFaceVerticesTextureVerticesBeginCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c);
void PolygonalFaceVerticesTextureVerticesVertexCallback(const obj::index_2_tuple_type &a);
void PolygonalFaceVerticesTextureVerticesEndCallback(void);
void PolygonalFaceVerticesNormalsBeginCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b,  const obj::index_2_tuple_type &c);
void PolygonalFaceVerticesNormalsCallback(const obj::index_2_tuple_type &a);
void PolygonalFaceVerticesNormalsEndCallback(void);
void PolygonalFaceVerticesTextureVerticesNormalsBeginCallback(const obj::index_3_tuple_type &a, const obj::index_3_tuple_type &b, const obj::index_3_tuple_type &c);
void PolygonalFaceVerticesTextureVerticesNormalsCallback(const obj::index_3_tuple_type &a);
void PolygonalFaceVerticesTextureVerticesNormalsEndCallback(void);


bool LoadMeshFromFile_libobj(const char *file, CMesh *mesh)
{
	mesh->Create();
	return MergeWithFile_libobj(file, mesh);
}

bool MergeWithFile_libobj(const char *file, CMesh *mesh)
{
	obj::obj_parser parser;
	bool r;

	loading_mesh = mesh;

	vertices.clear();
	uv_coords.clear();

	parser.geometric_vertex_callback(GeometryVertexCallback);
	parser.texture_vertex_callback(TextureVertexCallack);
	parser.face_callbacks(TriangularFaceVerticesCallback, TriangularFaceVerticesTextureVerticesCallback,
			      TriangularFaceVerticesNormalsCallback, TriangularFaceVerticesTextureVerticesNormalsCallback,
			      QuadrilateralFaceVerticesCallback, QuadrilateralFaceVerticesTextureVerticesCallback,
			      QuadrilateralFaceVerticesNormalsCallback, QuadrilateralFaceVerticesTextureVerticesNormalsCallback,
			      PolygonalFaceVerticesBeginCallback, PolygonalFaceVerticesVertexCallback, PolygonalFaceVerticesEndCallback,
			      PolygonalFaceVerticesTextureVerticesBeginCallback, PolygonalFaceVerticesTextureVerticesVertexCallback, PolygonalFaceVerticesTextureVerticesEndCallback,
			      PolygonalFaceVerticesNormalsBeginCallback, PolygonalFaceVerticesNormalsCallback, PolygonalFaceVerticesNormalsEndCallback,
			      PolygonalFaceVerticesTextureVerticesNormalsBeginCallback, PolygonalFaceVerticesTextureVerticesNormalsCallback, PolygonalFaceVerticesTextureVerticesNormalsEndCallback);
	r = parser.parse(file);
	loading_mesh->CalculateNormalsSolid();
	loading_mesh = 0;
	vertices.clear();
	uv_coords.clear();
	return r;
}

void GeometryVertexCallback(obj::float_type x, obj::float_type y, obj::float_type z)
{
	CVertex *v = new CVertex(Vec(x, y, z), loading_mesh);
	vertices.push_back(v);
}

void TextureVertexCallack(obj::float_type x, obj::float_type y)
{
	uv_coords.push_back(Vec(x, y));
}

void TriangularFaceVerticesCallback(obj::index_type a, obj::index_type b, obj::index_type c)
{
	if((long)a > (long)vertices.size() || (long)b > (long)vertices.size() || (long)c > (long)vertices.size())
	{
		printf("Invalid Index in OBJ-File\n");
		return;
	}

	CVertex *v[3];
	CTriangle *t;

	v[0] = vertices.at(a-1);
	v[1] = vertices.at(b-1);
	v[2] = vertices.at(c-1);

	t = new CTriangle(loading_mesh);
	t->Set(v[0], v[1], v[2], Vec(1.0, 1.0, 1.0), NullVec(), NullVec(), NullVec());
	t->SetMaterial(Vec(0.0, 0.0, 0.0), Vec(0.0, 0.0, 0.0), Vec(0.0, 0.0, 0.0), "$NONE");
}

void TriangularFaceVerticesTextureVerticesCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c)
{
	if((long)tr1::get<0>(a) > (long)vertices.size()
			|| (long)tr1::get<0>(b) > (long)vertices.size()
			|| (long)tr1::get<0>(c) > (long)vertices.size()
			|| (long)tr1::get<1>(a) > (long)uv_coords.size()
			|| (long)tr1::get<1>(b) > (long)uv_coords.size()
			|| (long)tr1::get<1>(c) > (long)uv_coords.size())
	{
		printf("Invalid Index in OBJ-File\n");
		return;
	}

	CVertex *v[3];
	CVector2 uv[3];
	CTriangle *t;

	v[0] = vertices.at(tr1::get<0>(a)-1);
	v[1] = vertices.at(tr1::get<0>(b)-1);
	v[2] = vertices.at(tr1::get<0>(c)-1);
	v[0]->uv = uv[0] = uv_coords.at(tr1::get<1>(a)-1);
	v[1]->uv = uv[1] = uv_coords.at(tr1::get<1>(b)-1);
	v[2]->uv = uv[2] = uv_coords.at(tr1::get<1>(c)-1);

	t = new CTriangle(loading_mesh);
	t->v[0] = v[0]; t->v[1] = v[1]; t->v[2] = v[2];
	t->color = Vec(1.0, 1.0, 1.0);
	t->SetMaterial(Vec(uv[0].x, uv[0].y, 0.0), Vec(uv[1].x, uv[1].y, 0.0), Vec(uv[2].x, uv[2].y, 0.0), "$NONE");
}

void TriangularFaceVerticesNormalsCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c) // TODO: Read Normals
{
	if((long)tr1::get<0>(a) > (long)vertices.size()
			|| (long)tr1::get<0>(b) > (long)vertices.size()
			|| (long)tr1::get<0>(c) > (long)vertices.size())
	{
		printf("Invalid Index in OBJ-File\n");
		return;
	}

	CVertex *v[3];
	CTriangle *t;

	v[0] = vertices.at(tr1::get<0>(a)-1);
	v[1] = vertices.at(tr1::get<0>(b)-1);
	v[2] = vertices.at(tr1::get<0>(c)-1);

	t = new CTriangle(loading_mesh);
	t->Set(v[0], v[1], v[2], Vec(1.0, 1.0, 1.0), NullVec(), NullVec(), NullVec());
}

void TriangularFaceVerticesTextureVerticesNormalsCallback(const obj::index_3_tuple_type &a, const obj::index_3_tuple_type &b, const obj::index_3_tuple_type &c) // TODO: Read Normals
{
	if((long)tr1::get<0>(a) > (long)vertices.size()
			|| (long)tr1::get<0>(b) > (long)vertices.size()
			|| (long)tr1::get<0>(c) > (long)vertices.size()
			|| (long)tr1::get<1>(a) > (long)uv_coords.size()
			|| (long)tr1::get<1>(b) > (long)uv_coords.size()
			|| (long)tr1::get<1>(c) > (long)uv_coords.size())
	{
		printf("Invalid Index in OBJ-File\n");
		return;
	}

	CVertex *v[3];
	CVector2 uv[3];
	CTriangle *t;

	v[0] = vertices.at(tr1::get<0>(a)-1);
	v[1] = vertices.at(tr1::get<0>(b)-1);
	v[2] = vertices.at(tr1::get<0>(c)-1);
	uv[0] = uv_coords.at(tr1::get<1>(a)-1);
	uv[1] = uv_coords.at(tr1::get<1>(b)-1);
	uv[2] = uv_coords.at(tr1::get<1>(c)-1);

	t = new CTriangle(loading_mesh);
	t->v[0] = v[0]; t->v[1] = v[1]; t->v[2] = v[2];
	t->color = Vec(1.0, 1.0, 1.0);
	t->SetMaterial(Vec(uv[0].x, uv[0].y, 0.0), Vec(uv[1].x, uv[1].y, 0.0), Vec(uv[2].x, uv[2].y, 0.0), "$NONE");
}

void QuadrilateralFaceVerticesCallback(obj::index_type a, obj::index_type b, obj::index_type c, obj::index_type d)
{
	if((long)a > (long)vertices.size()
			|| (long)b > (long)vertices.size()
			|| (long)c > (long)vertices.size()
			|| (long)d > (long)vertices.size())
	{
		printf("Invalid Index in OBJ-File\n");
		return;
	}

	CVertex *v[4];
	CTriangle *t;

	v[0] = vertices.at(a-1);
	v[1] = vertices.at(b-1);
	v[2] = vertices.at(c-1);
	v[3] = vertices.at(d-1);

	t = new CTriangle(loading_mesh);
	t->v[0] = v[0]; t->v[1] = v[1]; t->v[2] = v[2];
	t->color = Vec(1.0, 1.0, 1.0);
	t->SetMaterial(NullVec(), NullVec(), NullVec(), "$NONE");

	t = new CTriangle(loading_mesh);
	t->v[0] = v[2]; t->v[1] = v[3]; t->v[2] = v[0];
	t->color = Vec(1.0, 1.0, 1.0);
	t->SetMaterial(NullVec(), NullVec(), NullVec(), "$NONE");
}

void QuadrilateralFaceVerticesTextureVerticesCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c, const obj::index_2_tuple_type &d)
{
	if((long)tr1::get<0>(a) > (long)vertices.size()
			|| (long)tr1::get<0>(b) > (long)vertices.size()
			|| (long)tr1::get<0>(c) > (long)vertices.size()
			|| (long)tr1::get<0>(d) > (long)vertices.size()
			|| (long)tr1::get<1>(a) > (long)uv_coords.size()
			|| (long)tr1::get<1>(b) > (long)uv_coords.size()
			|| (long)tr1::get<1>(c) > (long)uv_coords.size()
			|| (long)tr1::get<1>(d) > (long)uv_coords.size())
	{
		printf("Invalid Index in OBJ-File\n");
		return;
	}

	CVertex *v[4];
	CVector2 uv[4];
	CTriangle *t;

	v[0] = vertices.at(tr1::get<0>(a)-1);
	v[1] = vertices.at(tr1::get<0>(b)-1);
	v[2] = vertices.at(tr1::get<0>(c)-1);
	v[3] = vertices.at(tr1::get<0>(d)-1);
	uv[0] = uv_coords.at(tr1::get<1>(a)-1);
	uv[1] = uv_coords.at(tr1::get<1>(b)-1);
	uv[2] = uv_coords.at(tr1::get<1>(c)-1);
	uv[3] = uv_coords.at(tr1::get<1>(d)-1);

	t = new CTriangle(loading_mesh);
	t->v[0] = v[0]; t->v[1] = v[1]; t->v[2] = v[2];
	t->color = Vec(1.0, 1.0, 1.0);
	t->SetMaterial(Vec(uv[0].x, uv[0].y, 0.0), Vec(uv[1].x, uv[1].y, 0.0), Vec(uv[2].x, uv[2].y, 0.0), "$NONE");

	t = new CTriangle(loading_mesh);
	t->v[0] = v[2]; t->v[1] = v[3]; t->v[2] = v[0];
	t->color = Vec(1.0, 1.0, 1.0);
	t->SetMaterial(Vec(uv[2].x, uv[2].y, 0.0), Vec(uv[3].x, uv[3].y, 0.0), Vec(uv[0].x, uv[0].y, 0.0), "$NONE");
}

void QuadrilateralFaceVerticesNormalsCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c, const obj::index_2_tuple_type &d)
{
}

void QuadrilateralFaceVerticesTextureVerticesNormalsCallback(const obj::index_3_tuple_type &a, const obj::index_3_tuple_type &b, const obj::index_3_tuple_type &c, const obj::index_3_tuple_type &d)
{
}


void PolygonalFaceVerticesBeginCallback(obj::index_type a, obj::index_type b, obj::index_type c)
{
	printf("Polygonal Face Import not implemented yet! Please convert all polygons in your OBJ File to Triangles (e.g. using Blender)\n");
}

void PolygonalFaceVerticesVertexCallback(obj::index_type a)
{
}

void PolygonalFaceVerticesEndCallback(void)
{
}

void PolygonalFaceVerticesTextureVerticesBeginCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b, const obj::index_2_tuple_type &c)
{
	printf("Polygonal Face Import not implemented yet! Please convert all polygons in your OBJ File to Triangles (e.g. using Blender)\n");
}

void PolygonalFaceVerticesTextureVerticesVertexCallback(const obj::index_2_tuple_type &a)
{
}

void PolygonalFaceVerticesTextureVerticesEndCallback(void)
{
}

void PolygonalFaceVerticesNormalsBeginCallback(const obj::index_2_tuple_type &a, const obj::index_2_tuple_type &b,  const obj::index_2_tuple_type &c)
{
	printf("Polygonal Face Import not implemented yet! Please convert all polygons in your OBJ File to Triangles (e.g. using Blender)\n");
}

void PolygonalFaceVerticesNormalsCallback(const obj::index_2_tuple_type &a)
{
}

void PolygonalFaceVerticesNormalsEndCallback(void)
{
}

void PolygonalFaceVerticesTextureVerticesNormalsBeginCallback(const obj::index_3_tuple_type &a, const obj::index_3_tuple_type &b, const obj::index_3_tuple_type &c)
{
	printf("Polygonal Face Import not implemented yet! Please convert all polygons in your OBJ File to Triangles (e.g. using Blender)\n");
}

void PolygonalFaceVerticesTextureVerticesNormalsCallback(const obj::index_3_tuple_type &a)
{
}

void PolygonalFaceVerticesTextureVerticesNormalsEndCallback(void)
{
}
