#include "allinc.h"

MainWindow::MainWindow(Editor *editor, QWidget *parent) : QMainWindow(parent)
{
	setupUi(this);

	this->editor = editor;

	tabifyDockWidget(TextureDockWidget, TriangleDockWidget);
	tabifyDockWidget(TextureDockWidget, EntityDockWidget);
	tabifyDockWidget(AnimationDockWidget, PositionDockWidget);

	RefreshPreviewSettingsCheck();

	InitGL();

	connect(AboutQtAction, SIGNAL(triggered()), this, SLOT(AboutQt()));
	connect(AboutTowerEditAction, SIGNAL(triggered()), this, SLOT(AboutTowerEdit()));

	connect(OpenAction, SIGNAL(triggered()), this, SLOT(Open()));
	connect(SaveAction, SIGNAL(triggered()), this, SLOT(Save()));
	connect(SaveAsAction, SIGNAL(triggered()), this, SLOT(SaveAs()));

	connect(ImportLib3DSAction, SIGNAL(triggered()), this, SLOT(ImportLib3DS()));
	connect(ImportLibG3DAction, SIGNAL(triggered()), this, SLOT(ImportLibG3D()));
	connect(ImportLibOBJAction, SIGNAL(triggered()), this, SLOT(ImportLibOBJ()));
	connect(MergeLib3DSAction, SIGNAL(triggered()), this, SLOT(MergeLib3DS()));
	connect(MergeLibG3DAction, SIGNAL(triggered()), this, SLOT(MergeLibG3D()));
	connect(MergeLibOBJAction, SIGNAL(triggered()), this, SLOT(MergeLibOBJ()));

	connect(ApplyMButton, SIGNAL(clicked()), this, SLOT(ApplyM()));
	connect(ApplyTButton, SIGNAL(clicked()), this, SLOT(ApplyT()));

	connect(DeleteTButton, SIGNAL(clicked()), this, SLOT(DeleteTriangle()));
	connect(EditTButton, SIGNAL(toggled(bool)), this, SLOT(EditTButtonC(bool)));
	connect(EditMButton, SIGNAL(toggled(bool)), this, SLOT(EditMButtonC(bool)));
	connect(NewTButton, SIGNAL(toggled(bool)), this, SLOT(NewTButtonC(bool)));
	connect(NewMButton, SIGNAL(toggled(bool)), this, SLOT(NewMButtonC(bool)));
	connect(CloneTButton, SIGNAL(clicked()), this, SLOT(CloneTButtonC()));
	connect(SetMaterialButton, SIGNAL(clicked()), this, SLOT(SetMaterial()));

	connect(SelectButton, SIGNAL(clicked()), this, SLOT(SelectButtonC()));
	connect(MoveButton, SIGNAL(clicked()), this, SLOT(MoveButtonC()));
	connect(ScaleButton, SIGNAL(clicked()), this, SLOT(ScaleButtonC()));

	connect(TriangleList, SIGNAL(itemSelectionChanged()), this, SLOT(SelectionChange()));
	connect(TextureList, SIGNAL(itemSelectionChanged()), this, SLOT(SelectionChange()));
	connect(EntityList, SIGNAL(itemSelectionChanged()), this, SLOT(RefreshEntityAttributeList()));

	connect(ShowAxesCheckBox, SIGNAL(toggled(bool)), this, SLOT(RefreshPreviewSettings()));
	connect(BothSidesCheckBox, SIGNAL(toggled(bool)), this, SLOT(RefreshPreviewSettings()));
	connect(WireframeCheckBox, SIGNAL(toggled(bool)), this, SLOT(RefreshPreviewSettings()));
	connect(ShowNormalsCheckBox, SIGNAL(toggled(bool)), this, SLOT(RefreshPreviewSettings()));

	connect(VertexSelectRadio, SIGNAL(toggled(bool)), this, SLOT(ChangeSelectModeC()));
	connect(TriangleSelectRadio, SIGNAL(toggled(bool)), this, SLOT(ChangeSelectModeC()));
	connect(GroupSelectRadio, SIGNAL(toggled(bool)), this, SLOT(ChangeSelectModeC()));

	connect(X1, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(X2, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(X3, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(Y1, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(Y2, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(Y3, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(Z1, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(Z2, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(Z3, SIGNAL(editingFinished()), this, SLOT(TEdit()));

	connect(TX1, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(TX2, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(TX3, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(TY1, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(TY2, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(TY3, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(TZ1, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(TZ2, SIGNAL(editingFinished()), this, SLOT(TEdit()));
	connect(TZ3, SIGNAL(editingFinished()), this, SLOT(TEdit()));

	connect(TextureMapperAction, SIGNAL(triggered()), this, SLOT(OpenTextureMapper()));
	connect(MatrixAction, SIGNAL(triggered()), this, SLOT(OpenMatrixDialog()));
	connect(FlipTrianglesAction, SIGNAL(triggered()), this, SLOT(FlipSelectedT()));
	connect(SeperateTrianglesAction, SIGNAL(triggered()), this, SLOT(SeperateTriangles()));
	connect(CleanUpMeshAction, SIGNAL(triggered()), this, SLOT(CleanUpMesh()));
	connect(CreateFlippedTrianglesAction, SIGNAL(triggered()), this, SLOT(CreateFlippedTriangles()));
	connect(SimplifyMeshAction, SIGNAL(triggered()), this, SLOT(SimplifyMesh()));
	connect(CalculateNormalsAction, SIGNAL(triggered()), this, SLOT(CalculateNormals()));

	connect(TriangleDockWidget, SIGNAL(visibilityChanged(bool)), this, SLOT(RefreshWidgetActions()));
	connect(TextureDockWidget, SIGNAL(visibilityChanged(bool)), this, SLOT(RefreshWidgetActions()));
	connect(EntityDockWidget, SIGNAL(visibilityChanged(bool)), this, SLOT(RefreshWidgetActions()));
	connect(PositionDockWidget, SIGNAL(visibilityChanged(bool)), this, SLOT(RefreshWidgetActions()));
	connect(PreviewSettingsDockWidget, SIGNAL(visibilityChanged(bool)), this, SLOT(RefreshWidgetActions()));

	connect(TriangleWidgetAction, SIGNAL(triggered()), this, SLOT(RefreshWidgets()));
	connect(TextureWidgetAction, SIGNAL(triggered()), this, SLOT(RefreshWidgets()));
	connect(EntityWidgetAction, SIGNAL(triggered()), this, SLOT(RefreshWidgets()));
	connect(PositionWidgetAction, SIGNAL(triggered()), this, SLOT(RefreshWidgets()));
	connect(AnimationWidgetAction, SIGNAL(triggered()), this, SLOT(RefreshWidgets()));
	connect(PreviewSettingsWidgetAction, SIGNAL(triggered()), this, SLOT(RefreshWidgets()));

	connect(NewPositionButton, SIGNAL(clicked()), this, SLOT(NewPosition()));
	connect(DeletePositionButton, SIGNAL(clicked()), this, SLOT(DeletePosition()));
	connect(PositionComboBox, SIGNAL(activated(const QString)), this, SLOT(ChangePosition(const QString)));

	connect(AnimationModeCheckBox, SIGNAL(toggled(bool)), this, SLOT(RefreshAnimation()));
	connect(NewAnimationButton, SIGNAL(clicked()), this, SLOT(NewAnimation()));
	connect(DeleteAnimationButton, SIGNAL(clicked()), this, SLOT(DeleteAnimation()));
	connect(NewKeyFrameButton, SIGNAL(clicked()), this, SLOT(NewKeyFrame()));
	connect(DeleteKeyFrameButton, SIGNAL(clicked()), this, SLOT(DeleteKeyFrame()));
	connect(KeyFrameList, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(JumpToKeyFrame()));
	connect(EditKeyFrameButton, SIGNAL(clicked()), this, SLOT(RefreshAnimation()));
	connect(TimeLine, SIGNAL(sliderMoved(int)), this, SLOT(RefreshAnimationTime(int)));
	connect(TimeLine, SIGNAL(sliderReleased()), this, SLOT(RefreshAnimationTime()));

	connect(EditEntityAttributeButton, SIGNAL(clicked()), this, SLOT(OpenEntityAttributeEditor()));
	connect(RenameEntityButton, SIGNAL(clicked()), this, SLOT(RenameEntity()));
	connect(SetEntityGroupButton, SIGNAL(clicked()), this, SLOT(SetEntityGroup()));
	connect(NewEntityButton, SIGNAL(clicked()), this, SLOT(NewEntity()));
	connect(NewEntityAttributeButton, SIGNAL(clicked()), this, SLOT(NewEntityAttribute()));
	connect(DeleteEntityButton, SIGNAL(clicked()), this, SLOT(DeleteEntity()));
	connect(DeleteEntityAttributeButton, SIGNAL(clicked()), this, SLOT(DeleteEntityAttribute()));

	connect(MeshInfoAction, SIGNAL(triggered()), this, SLOT(ShowMeshInfo()));

	TriangleDockWidget->close();
	TextureDockWidget->show();
	PositionDockWidget->close();
	AnimationDockWidget->close();
	PreviewSettingsDockWidget->close();
	EntityDockWidget->close();

	status_progress_bar = new QProgressBar(this);
	status_progress_bar->setMinimum(0);
	status_progress_bar->setMaximum(1000); // resolution
	StatusBar->addPermanentWidget(status_progress_bar);
	status_progress_bar->hide();

	mode = MODE_NONE;
	edit_mode = real_edit_mode = EDIT_MODE_SELECT;
	k_edit_mode = EDIT_MODE_NONE;

	edit_triangle = 0;
	edit_material = 0;
	edit_anim = 0;
	key_list = 0;
	edit_key = 0;

	tex_nr = 0;
	tri_nr = 0;

	apply_selection = 1;

	texture_mapper = 0;

	preview2d[0] = preview2d[1] = preview2d[2] = 0;

	RefreshLists();
	RefreshMode();

	RefreshTitle();

	Refresh2DPreviewMode();
	RefreshAnimation();

	resize(1024, 768);

	startTimer(TIMER_INTERVAL);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	QMainWindow::closeEvent(event);
	close();
	exit(0);
}

void MainWindow::timerEvent(QTimerEvent *event)
{
	QMainWindow::timerEvent(event);

	if(texture_mapper != 0 && !texture_mapper->isVisible())
	{
		delete texture_mapper;
		texture_mapper = 0;
	}

	for(int i=0; i<3; i++)
		if(preview2d[i]->selection_changed)
		{
			preview2d[i]->selection_changed = 0;
			RefreshMode();
			RefreshLists();
		}

	real_edit_mode = k_edit_mode != EDIT_MODE_NONE ? k_edit_mode : edit_mode;

	switch(real_edit_mode)
	{
	case EDIT_MODE_SELECT:
		SelectButton->setChecked(true);
		MoveButton->setChecked(false);
		ScaleButton->setChecked(false);
		break;
	case EDIT_MODE_MOVE:
		SelectButton->setChecked(false);
		MoveButton->setChecked(true);
		ScaleButton->setChecked(false);
		break;
	case EDIT_MODE_SCALE:
		SelectButton->setChecked(false);
		MoveButton->setChecked(false);
		ScaleButton->setChecked(true);
		break;
	}

	if(editor->GetAnimationMode() && PlayAnimationButton->isChecked())
	{
		RefreshAnimation((float)TIMER_INTERVAL / 1000.0);
	}

	Refresh2DPreview();
	RefreshUi();

	gl_win->CalculateCamPos();
	gl_win->updateGL();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
	QMainWindow::keyPressEvent(event);
	switch(event->key())
	{
	case Qt::Key_C:
		editor->GetMesh()->Create();
		break;
		case Qt::Key_S:
			k_edit_mode = EDIT_MODE_SELECT;
			break;
		case Qt::Key_M:
			k_edit_mode = EDIT_MODE_MOVE;
			break;
		case Qt::Key_R:
			k_edit_mode = EDIT_MODE_SCALE;
			break;
	case Qt::Key_V:
		gl_win->ResetFocus();
		break;
	case Qt::Key_Delete:
		if((editor->GetSelection()->select_mode == SELECT_MODE_VERTEX && editor->GetSelection()->selected_v.size() == 0)
				|| ((editor->GetSelection()->select_mode == SELECT_MODE_GROUP || editor->GetSelection()->select_mode == SELECT_MODE_TRIANGLE)
					&& editor->GetSelection()->selected_t.size() == 0))
			break;
		if(QMessageBox::question(this, "Delete Selection",
					   "Are you sure that you want to delete the selected Vertices/Triangles?",
					   QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
			break;
		editor->GetSelection()->DeleteSelection();
		SetPreviewRedraw();
		break;
	}
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
	QMainWindow::keyPressEvent(event);
	switch(event->key())
	{
	case Qt::Key_S:
	case Qt::Key_M:
		k_edit_mode = EDIT_MODE_NONE;
		break;
	}
}

MainWindow::~MainWindow()
{
	exit(0);
}

void MainWindow::AboutQt(void)
{
	QMessageBox::aboutQt(this, "About Qt");
}

void MainWindow::AboutTowerEdit(void)
{
	QMessageBox::about(this, "About TowerEdit",	"<h2>TowerEdit</h2>for TowerEngine<br><br>"
												"Copyright (C) 2011  Metallic Entertainment (Florian M&auml;rkl)<br>"
												"<br>"
												"This program is free software: you can redistribute it and/or modify "
												"it under the terms of the GNU General Public License as published by "
												"the Free Software Foundation, version 3 of the License"
												"<br>"
												"This program is distributed in the hope that it will be useful, "
												"but WITHOUT ANY WARRANTY; without even the implied warranty of "
												"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
												"GNU General Public License for more details.<br>"
												"<br>"
												"For a copy of this license see <a href=\"http://www.gnu.org/licenses/\">http://www.gnu.org/licenses/</a>.");
}

void MainWindow::InitGL(void)
{
	ilInit();
	iluInit();

	gl_win = new GLWin(editor, this);
	GLLayout->addWidget(gl_win);
	gl_win->show();
}

void MainWindow::Open(void)
{
	QString file_name;
	const char *file_name_c;
	char *message;
	size_t l;

	if(!gl_win)
	{
		QMessageBox::critical(this, "Open Mesh", "OpenGL is not initialized.");
		return;
	}

	file_name = QFileDialog::getOpenFileName(this, tr("Open Mesh"), tr("."), tr(""));

	if(file_name.length() == 0)
		return;

	editor->GetSelection()->DeleteSelection();

	file_name_c = file_name.toLocal8Bit().constData();
	message = new char[l = strlen(file_name_c) + strlen("Loading ...") + 1];
	snprintf(message, l-1, "Loading %s...", file_name_c);

	editor->SetBusy(true);
	editor->SetStatusMessage(true, message);
	editor->SetProgress(false);
	RefreshApp();

	editor->Load(file_name.toLocal8Bit().constData());

	editor->SetBusy(false);
	RefreshApp();
}


void MainWindow::ImportLib3DS(int merge)
{
	QString file_name;

	if(!gl_win)
	{
		QMessageBox::critical(this, "Import Mesh", "OpenGL is not initialized.");
		return;
	}

	file_name = QFileDialog::getOpenFileName(this, tr("Import using Lib3ds"), tr("."), tr(""));

	if(file_name.length() == 0)
		return;

	editor->GetSelection()->DeleteSelection();

	if(merge)
		editor->GetMesh()->MergeWithFile_lib3ds(file_name.toLocal8Bit().constData());
	else
	{
		editor->GetMesh()->LoadFromFile_lib3ds(file_name.toLocal8Bit().constData());
		editor->SetFileName(0);
	}

	RefreshTitle();
	RefreshLists();
}

void MainWindow::ImportLibG3D(int merge)
{
	QString file_name;

	if(!gl_win)
	{
		QMessageBox::critical(this, "Import Mesh", "OpenGL is not initialized.");
		return;
	}

	file_name = QFileDialog::getOpenFileName(this, tr("Import using LibG3D"), tr("."), tr(""));

	if(file_name.length() == 0)
		return;

	editor->GetSelection()->DeleteSelection();

	if(merge)
		editor->GetMesh()->MergeWithFile_libg3d(file_name.toLocal8Bit().constData());
	else
	{
		editor->GetMesh()->LoadFromFile_libg3d(file_name.toLocal8Bit().constData());
		editor->SetFileName(0);
	}


	RefreshTitle();
	RefreshLists();
}

void MainWindow::ImportLibOBJ(int merge)
{
	QString file_name;

	if(!gl_win)
	{
		QMessageBox::critical(this, "Import Mesh", "OpenGL is not initialized.");
		return;
	}

	file_name = QFileDialog::getOpenFileName(this, tr("Import using LibOBJ"), tr("."), tr(""));

	if(file_name.length() == 0)
		return;

	editor->GetSelection()->DeleteSelection();

	/*if(merge)
		editor->GetMesh()->MergeWithFile_libg3d(file_name.toLocal8Bit().constData());
	else
	{
		editor->GetMesh()->LoadFromFile_libg3d(file_name.toLocal8Bit().constData());
		editor->SetFileName(0);
	}*/

	editor->LoadFromFile_libobj(file_name.toLocal8Bit().constData(), merge);

	RefreshTitle();
	RefreshLists();
}

void MainWindow::SaveAs(void)
{
	QString file_name;

	if(!gl_win)
	{
		QMessageBox::critical(this, "Save Mesh", "OpenGL is not initialized.");
		return;
	}

	if(!editor->GetMesh()->GetState())
	{
		QMessageBox::critical(this, "Save Mesh", "Nothing to save.");
		return;
	}

	file_name = QFileDialog::getSaveFileName(this, tr("Save Mesh"), ".", tr(""));

	if(file_name.length() == 0)
	   return;

	editor->GetMesh()->SaveToFile(file_name.toAscii());

	editor->SetFileName(file_name.toLocal8Bit().constData());
	RefreshTitle();
}

void MainWindow::Save(void)
{
	if(!editor->GetFileName())
	{
		SaveAs();
		return;
	}
	editor->GetMesh()->SaveToFile(editor->GetFileName());
	RefreshTitle();
}

void MainWindow::RefreshUi(void)
{
	if(editor->GetBusy())
	{
		if(editor->GetProgressEnabled())
		{
			status_progress_bar->setValue(status_progress_bar->maximum() * editor->GetProgress());
			status_progress_bar->show();
		}
		else
			status_progress_bar->hide();

		if(editor->GetStatusMessageEnabled())
			StatusBar->showMessage(editor->GetStatusMessage());
		else
			StatusBar->showMessage(tr("working..."));
	}
	else
	{
		StatusBar->clearMessage();
		status_progress_bar->hide();
	}

	menuBar()->setDisabled(editor->GetBusy());
	centralWidget()->setDisabled(editor->GetBusy());
}

void MainWindow::RefreshWidgetActions(void)
{
	SyncDockAction(TriangleDockWidget, TriangleWidgetAction, 0);
	SyncDockAction(TextureDockWidget, TextureWidgetAction, 0);
	SyncDockAction(PositionDockWidget, PositionWidgetAction, 0);
	SyncDockAction(AnimationDockWidget, AnimationWidgetAction, 0);
	SyncDockAction(PreviewSettingsDockWidget, PreviewSettingsWidgetAction, 0);
	SyncDockAction(EntityDockWidget, EntityWidgetAction, 0);
}

void MainWindow::RefreshWidgets(void)
{
	SyncDockAction(TriangleDockWidget, TriangleWidgetAction, 1);
	SyncDockAction(TextureDockWidget, TextureWidgetAction, 1);
	SyncDockAction(PositionDockWidget, PositionWidgetAction, 1);
	SyncDockAction(AnimationDockWidget, AnimationWidgetAction, 1);
	SyncDockAction(PreviewSettingsDockWidget, PreviewSettingsWidgetAction, 1);
	SyncDockAction(EntityDockWidget, EntityWidgetAction, 1);
}

void MainWindow::RefreshPositionList(void)
{
	int i;
	char *current_name = 0;

	PositionComboBox->clear();
	PositionComboBox->addItem("Idle");

	for(i=0; i<editor->GetMesh()->GetCustomPositionCount(); i++)
		PositionComboBox->addItem(editor->GetMesh()->GetCustomPosition(i)->name);

	if(editor->GetMesh()->GetCurrentPosition() != edit_key)
		current_name = editor->GetMesh()->GetCurrentPositionName();
	if(!current_name)
		PositionComboBox->setCurrentIndex(0);
	else
	{
		for(i=1; i<PositionComboBox->count(); i++)
			if(strcmp(PositionComboBox->itemText(i).toLocal8Bit().constData(), current_name) == 0)
			{
				PositionComboBox->setCurrentIndex(i);
				break;
			}

	}

	if(editor->GetMesh()->GetCurrentPosition() == editor->GetMesh()->idle_position)
		DeletePositionButton->setEnabled(false);
	else
		DeletePositionButton->setEnabled(true);

}

void MainWindow::NewPosition(void)
{
	QString name;
	char *_name;
	CCustomPosition *p;

	name = QInputDialog::getText(this, "New Position", "Name: ");
	if(name.length() == 0)
		return;

	_name = name.toLocal8Bit().data();

	if(strcmp(_name, "Idle") == 0)
	{
		QMessageBox::critical(this, "New Position", "A custom position cannot be called \"Idle\"!");
		return;
	}
	if(editor->GetMesh()->GetPositionByName(_name))
	{
		QMessageBox::critical(this, "New Position", "The name has already been taken!");
		return;
	}

	p = editor->GetMesh()->CreatePosition(_name);
	if(p)
		ChangePosition(p);

	RefreshLists();
}

void MainWindow::DeletePosition(void)
{
	CMeshPosition *p;

	p = editor->GetMesh()->GetCurrentPosition();
	if(p != editor->GetMesh()->idle_position)
		delete (CCustomPosition *)p;

	RefreshLists();
}

void MainWindow::ChangePosition(const QString text)
{
	if(editor->GetAnimationMode())
		return;

	editor->GetMesh()->CopyPositionFromVertices();
	editor->GetMesh()->ChangePosition(text.toLocal8Bit());
	if(editor->GetMesh()->GetCurrentPosition() == editor->GetMesh()->idle_position)
		DeletePositionButton->setEnabled(false);
	else
		DeletePositionButton->setEnabled(true);

	SetPreviewRedraw();
}

void MainWindow::ChangePosition(CMeshPosition *p)
{
	editor->GetMesh()->CopyPositionFromVertices();
	editor->GetMesh()->ChangePosition(p);
}

// #animation

void MainWindow::RefreshAnimation(float time)
{
	if(AnimationModeCheckBox->isChecked() && editor->GetMesh()->GetAnimationCount() > 0)
	{
		editor->SetAnimationMode(1);
		edit_key_mode = EditKeyFrameButton->isChecked();
		if(edit_key_mode && edit_key)
			ChangePosition((CMeshPosition *)edit_key);
		else
			ChangePosition((CMeshPosition *)0);
		if(time > 0.0)
		{
			editor->GetMesh()->PlayAnimation(time);
			TimeLine->setValue(editor->GetMesh()->GetCurrentAnimation()->GetTime() * TIME_LINE_PRECISION);
		}
		TimeLine->setEnabled(!edit_key_mode);
		edit_anim = editor->GetMesh()->GetAnimationByName(AnimationComboBox->currentText().toLocal8Bit().constData());
		if(edit_anim != editor->GetMesh()->GetCurrentAnimation())
			editor->GetMesh()->ChangeAnimation(edit_anim);
		AnimationEditWidget->setEnabled(true);
		AnimationComboBox->setEnabled(false);
	}
	else
	{
		if(editor->GetAnimationMode())
		{
			editor->SetAnimationMode(0);
			ChangePosition("Idle");
		}
		AnimationModeCheckBox->setChecked(false);
		edit_anim = 0;
		edit_key = 0;
		edit_key_mode = 0;
		editor->SetAnimationMode(0);
		AnimationEditWidget->setEnabled(false);
		AnimationComboBox->setEnabled(true);
		EditKeyFrameButton->setChecked(false);
	}

	RefreshAnimationList();
	RefreshAnimationWidget();

	if(edit_anim && editor->GetAnimationMode())
		edit_anim->ApplyCurrentFrame();

	SetPreviewRedraw();
}

void MainWindow::RefreshAnimationList(void)
{
	CAnimation *a;
	CKeyFrame *f;
	float t_min;
	float t_last;
	CKeyFrame *f_min;
	int key_c, i;
	int current_index;

	AnimationComboBox->clear();
	current_index = 0;
	for(i=0; i<editor->GetMesh()->GetAnimationCount(); i++)
	{
		a = editor->GetMesh()->GetAnimation(i);
		if(a==edit_anim)
			current_index = AnimationComboBox->count();
		AnimationComboBox->addItem(a->GetName());
	}
	if(editor->GetAnimationMode())
		AnimationComboBox->setCurrentIndex(current_index);


	KeyFrameList->clear();
	if(key_list)
		delete [] key_list;
	key_list = 0;
	if(edit_anim && editor->GetAnimationMode())
	{
		key_c = 0;
		for(f=edit_anim->key_first; f; f=f->chain_next)
			key_c++;
		key_list = new CKeyFrame *[key_c];
		i=0;

		t_last = -1.0;
		while(1)
		{
			t_min = INFINITY;
			f_min = 0;
			for(f=edit_anim->key_first; f; f=f->chain_next)
			{
				if(f->time < t_min && f->time > t_last)
				{
					t_min = f->time;
					f_min = f;
				}
			}

			if(f_min == 0)
				break;

			KeyFrameList->addItem(QString::number((double)f_min->time, 'f', 6) + "s");
			key_list[i] = f_min;
			if(edit_key && edit_key == f_min)
				KeyFrameList->setCurrentRow(i);
			i++;
			t_last = f_min->time;
		}
		if(!edit_key)
		{
			KeyFrameList->setCurrentRow(0);
			edit_key = key_list[0];
		}
	}

	DeleteKeyFrameButton->setEnabled(KeyFrameList->count() != 0);
	EditKeyFrameButton->setEnabled(KeyFrameList->count() != 0);
}

void MainWindow::RefreshAnimationWidget(void)
{
	char time_label[50];

	if(editor->GetAnimationMode() && edit_anim)
	{
		TimeLine->setMinimum(0);
		TimeLine->setMaximum(TIME_LINE_PRECISION * edit_anim->GetLength());
		snprintf(time_label, 49, "%fs / %fs", edit_anim->GetTime(), edit_anim->GetLength());
		TimeLabel->setText(QString::fromLocal8Bit(time_label));
	}

	DeleteAnimationButton->setEnabled(!editor->GetAnimationMode() && (AnimationComboBox->count() > 0));
	NewAnimationButton->setEnabled(!editor->GetAnimationMode());
	AnimationModeCheckBox->setEnabled(AnimationComboBox->count() > 0);
}

void MainWindow::RefreshAnimationTime(int time)
{
	if(!editor->GetAnimationMode() || !edit_anim)
		return;

	edit_anim->SetTime((float)time / (float)TIME_LINE_PRECISION);

	RefreshAnimation();
}

void MainWindow::RefreshAnimationTime(void) { RefreshAnimationTime(TimeLine->value()); }

void MainWindow::NewAnimation(void)
{
	QString name;
	char *_name;
	char *__name;
	float len;
	bool ok;
	CAnimation *p;

	name = QInputDialog::getText(this, "New Animation", "Name: ");
	if(name.length() == 0)
		return;

	_name = name.toLocal8Bit().data();
	__name = new char[strlen(_name) + 1];
	strcpy(__name, _name);

	if(editor->GetMesh()->GetPositionByName(_name))
	{
		QMessageBox::critical(this, "New Animation", "The name has already been taken!");
		return;
	}

	len = QInputDialog::getDouble(this, "New Animation", "Length (in seconds)", 1.0, 0.00001, 2147483647.0, 4, &ok);
	if(!ok)
		return;

	p = editor->GetMesh()->CreateAnimation(__name, len);
	if(p)
		editor->GetMesh()->ChangeAnimation(p);

	RefreshAnimation();
}

void MainWindow::DeleteAnimation(void)
{
	CAnimation *a = 0;

	if(editor->GetAnimationMode())
		return;

	a = editor->GetMesh()->GetAnimationByName(AnimationComboBox->currentText().toLocal8Bit().constData());
	if(!a)
		return;
	delete a;
	RefreshAnimation();
}

void MainWindow::NewKeyFrame(void)
{
	float time;
	bool ok;
	CKeyFrame *f;
	CKeyFrame *fi;

	if(!editor->GetAnimationMode() || !edit_anim)
		return;

	time = QInputDialog::getDouble(this, "New Keyframe", "Length (in seconds)", 1.0, 0.00001, 2147483647.0, 4, &ok);
	if(!ok)
		return;

	if(time < 0.0 || time > edit_anim->GetLength())
	{
		QMessageBox::critical(this, "New Keyframe", "Time out of range!");
			return;
	}

	for(fi=edit_anim->key_first; fi; fi=fi->chain_next)
		if(fi->time == time)
		{
			QMessageBox::critical(this, "New Keyframe", "Could not create the new Keyframe because there is already one at this time!");
			return;
		}

	f = edit_anim->NewKeyFrame(time);
	f->CopyFromVertices();

	RefreshAnimation();
}

void MainWindow::DeleteKeyFrame(void)
{
	int row;

	if(!editor->GetAnimationMode() || !edit_anim)
		return;

	if(!key_list)
		return;

	row = KeyFrameList->currentRow();
	if(row < 0)
		return;

	delete key_list[row];

	RefreshAnimation();
}

void MainWindow::JumpToKeyFrame(int r)
{
	if(!key_list || r < 0)
		return;
	TimeLine->setSliderPosition(key_list[r]->time * TIME_LINE_PRECISION);
	if(edit_key && edit_key_mode)
		edit_key->CopyFromVertices();
	edit_key = key_list[r];
	edit_anim->SetTime(edit_key->time);
	RefreshAnimation();
}

// #dock

void MainWindow::SyncDockAction(QDockWidget *d, QAction *a, int dir) // 0 means d -> a; 1 means the opposite
{
	if(dir == 0) // d -> a
		a->setChecked(!d->isHidden());
	else if(dir == 1)
	{
		if(a->isChecked())
		{
			if(!d->isVisible())
			{
				d->show();
				d->raise();
			}
		}
		else
			d->close();
	}
}

void MainWindow::RefreshTitle(void)
{
	char title[502];
	if(!editor->GetFileName() || !editor->GetMesh()->GetState())
		snprintf(title, 500, "TowerEdit for TowerEngine");
	else
	snprintf(title, 500, "TowerEdit for TowerEngine (%s)", editor->GetFileName());
	setWindowTitle(title);
}

void MainWindow::ChangeSelectModeC(void)
{
	if(VertexSelectRadio->isChecked())
	editor->GetSelection()->SwitchToVertexSelectMode();
	else if(TriangleSelectRadio->isChecked())
	editor->GetSelection()->SwitchToTriangleSelectMode();
	else if(GroupSelectRadio->isChecked())
	editor->GetSelection()->SwitchToGroupSelectMode();
	SeperateTrianglesAction->setEnabled(editor->GetSelection()->select_mode != SELECT_MODE_VERTEX);
	SetPreviewRedraw();
}



void MainWindow::CalculateNormals(void)
{
	normals_calculator = new NormalsCalculator(editor, this);
	normals_calculator->show();
}


void MainWindow::FlipSelectedT(void)
{
	set<CTriangle *>::iterator i;

	if(editor->GetSelection()->select_mode == SELECT_MODE_VERTEX)
		return;

	for(i=editor->GetSelection()->selected_t.begin(); i!=editor->GetSelection()->selected_t.end(); i++)
		(*i)->Flip();
}


void MainWindow::SeperateTriangles(void)
{
	set<CVertex *> used_v;
	int i, v;
	CMesh *mesh = editor->GetMesh();
	CVertex *o, *n;
	CTriangle *t;

	editor->SetBusy(true);
	editor->SetStatusMessage(true, "Seperating Triangles...");
	editor->SetProgress(true, 0.0);
	RefreshApp();

	for(i=0; i<mesh->GetTriangleCount(); i++)
	{
		t = mesh->GetTriangle(i);
		for(v=0; v<3; v++)
		{
			o = t->v[v];
			if(used_v.find(o) == used_v.end())
				used_v.insert(o);
			else
			{
				n = new CVertex(*o, mesh);
				n->normal = o->normal;
				n->uv = o->uv;
				t->v[v] = n;
			}
		}

		editor->SetProgress(true, (float)i / (float)mesh->GetTriangleCount());
		RefreshApp();
	}

	editor->SetBusy(false);
	RefreshApp();
	setEnabled(true);

}

void MainWindow::CleanUpMesh(void)
{
	set<CVertex *> used_v;
	int i;
	CMesh *mesh = editor->GetMesh();
	CTriangle *t;
	CVertex *v;

	for(i=0; i<mesh->GetTriangleCount(); i++)
	{
		t = mesh->GetTriangle(i);
		if(used_v.find(t->v[0]) == used_v.end())
			used_v.insert(t->v[0]);
		if(used_v.find(t->v[1]) == used_v.end())
			used_v.insert(t->v[1]);
		if(used_v.find(t->v[2]) == used_v.end())
			used_v.insert(t->v[2]);
	}

	for(i=0; i<mesh->GetVertexCount();)
	{
		v = mesh->GetVertex(i);
		if(used_v.find(v) == used_v.end())
			delete v;
		else
			i++;
	}
}

void MainWindow::SimplifyMesh(void)
{
	int i, j, k, l;
	CVertex *v, *o;
	CTriangle *t;
	CMesh *mesh = editor->GetMesh();
	bool replaced;

	CleanUpMesh();

	for(i=0; i<mesh->GetVertexCount();)
	{
		v = mesh->GetVertex(i);
		replaced = false;
		for(j=0; j<i; j++)
		{
			o = mesh->GetVertex(j);
			if((CVector)(*o) == (CVector)(*v)
				&& o->normal == v->normal
				&& o->uv == v->uv)
			{
				for(k=0; k<mesh->GetTriangleCount(); k++)
				{
					t = mesh->GetTriangle(k);
					for(l=0; l<3; l++)
						if(t->v[l] == v)
							t->v[l] = o;
				}
				delete v;
				replaced = true;
				break;
			}

		}
		if(!replaced)
			i++;
	}
}


void MainWindow::CreateFlippedTriangles(void)
{
	set<CTriangle *>::iterator i;
	char *mat;

	if(editor->GetSelection()->select_mode == SELECT_MODE_VERTEX)
		return;

	for(i=editor->GetSelection()->selected_t.begin(); i!=editor->GetSelection()->selected_t.end(); i++)
	{
		mat = new char[100];
		strcpy(mat, (*i)->m_name);
		editor->GetMesh()->CreateTriangle((*i)->v[0], (*i)->v[1], (*i)->v[2], (*i)->color, mat, (*i)->tex_coord[0], (*i)->tex_coord[1], (*i)->tex_coord[2])->Flip();
	}
}


void MainWindow::ShowMeshInfo(void)
{
	char text[501];
	CVector m = Vec(0.0, 0.0, 0.0);
	int c = 0;
	int l;
	set<CVertex *>::iterator vi;

	snprintf(text, 500, "Vertices: %d\nTriangles: %d\n\nSelection:\n",
		 editor->GetMesh()->GetVertexCount(),
		 editor->GetMesh()->GetTriangleCount());
	l = strlen(text);
	switch(editor->GetSelection()->select_mode)
	{
	case SELECT_MODE_VERTEX:
		for(vi = editor->GetSelection()->selected_v.begin(); vi != editor->GetSelection()->selected_v.end(); vi++)
		{
			m += **vi;
			c++;
		}
		m *= 1.0 / (float)c;
		snprintf(text + l, 500 - l, "%d Vertices\nMedian: %f, %f, %f",
			 (int)editor->GetSelection()->selected_v.size(), pvec(m));
		break;
	case SELECT_MODE_TRIANGLE:
	case SELECT_MODE_GROUP:
		snprintf(text + l, 500 - l, "%d Triangles", (int)editor->GetSelection()->selected_t.size());
		break;
	}

	QMessageBox::information(this, "Mesh Info", QString::fromLocal8Bit(text));
}


void MainWindow::ApplyT(void)
{
	if(!editor->GetMesh()->GetState())
	editor->GetMesh()->Create();
	switch(mode)
	{
	case MODE_EDIT_TRIANGLE:
		delete edit_triangle;
	case MODE_NEW_TRIANGLE:
	editor->GetMesh()->CreateTriangle(editor->GetMesh()->CreateVertex(Vec(X1->value(), Y1->value(), Z1->value())),
					  editor->GetMesh()->CreateVertex(Vec(X2->value(), Y2->value(), Z2->value())),
					  editor->GetMesh()->CreateVertex(Vec(X3->value(), Y3->value(), Z3->value())),
									  Vec(R->value(), G->value(), B->value()),
									  TTexName->lineEdit()->text().toAscii().data(),
									  Vec(TX1->value(), TY1->value(), TZ1->value()),
									  Vec(TX2->value(), TY2->value(), TZ2->value()),
									  Vec(TX3->value(), TY3->value(), TZ3->value()));
		break;
	}

	mode = MODE_NONE;
	RefreshMode();
	RefreshLists();

	EditTButton->setChecked(false);
	NewTButton->setChecked(false);
}

void MainWindow::ApplyM(void)
{
	char name[100];

	if(!editor->GetMesh()->GetState())
	editor->GetMesh()->Create();

	switch(mode)
	{
	case MODE_EDIT_MATERIAL:
		delete edit_material;
		void RefreshAnimationTime(void);
		void NewAnimation(void);
		void DeleteAnimation(void);
		void NewKeyFrame(void);
		void DeleteKeyFrame(void);
		void JumpToKeyFrame(int r);
	case MODE_NEW_MATERIAL:
		strcpy(name, MTexName->text().toLocal8Bit().constData());
	editor->GetMesh()->CreateMaterialRelative(PathOfFile(editor->GetFileName()), Diffuse->text().toLocal8Bit().constData(),
									Specular->text().toLocal8Bit().constData(),
									Exponent->value(), Normal->text().toLocal8Bit().constData(),
					AEB->text().toLocal8Bit().constData(), BumpFactor->value(),
					Height->text().toLocal8Bit().constData(), HeightFactor->value(),
									name);
	editor->GetMesh()->SetTriangleMaterials();
	}

	mode = MODE_NONE;
	RefreshMode();
	RefreshLists();

	EditMButton->setChecked(false);
	NewMButton->setChecked(false);
}

void MainWindow::RefreshPreviewSettings(void)
{
	editor->preview.axes = ShowAxesCheckBox->isChecked();
	editor->preview.both_sides = BothSidesCheckBox->isChecked();
	editor->preview.wireframe = WireframeCheckBox->isChecked();
	editor->preview.normals = ShowNormalsCheckBox->isChecked();
}

void MainWindow::RefreshPreviewSettingsCheck(void)
{
	ShowAxesCheckBox->setChecked(editor->preview.axes);
	BothSidesCheckBox->setChecked(editor->preview.both_sides);
	WireframeCheckBox->setChecked(editor->preview.wireframe);
	ShowNormalsCheckBox->setChecked(editor->preview.normals);
}

void MainWindow::Refresh2DPreview(void)
{
	int i;

	for(i=0; i<3; i++)
	{
		preview2d[i]->RefreshPreview();
	}
}

void MainWindow::Refresh2DPreviewMode(void)
{
	SetViewComboBox(Preview2d1Mode, PREVIEW_VIEW_FRONT);
	SetViewComboBox(Preview2d2Mode, PREVIEW_VIEW_TOP);
	SetViewComboBox(Preview2d3Mode, PREVIEW_VIEW_LEFT);

	preview2d[0] = new Preview2D(Preview2d1Mode, editor->GetSelection(), this);
	preview2d[1] = new Preview2D(Preview2d2Mode, editor->GetSelection(), this);
	preview2d[2] = new Preview2D(Preview2d3Mode, editor->GetSelection(), this);

	Preview2d1->addWidget(preview2d[0]);
	Preview2d2->addWidget(preview2d[1]);
	Preview2d3->addWidget(preview2d[2]);

	preview2d[0]->SetMesh(editor->GetMesh());
	preview2d[1]->SetMesh(editor->GetMesh());
	preview2d[2]->SetMesh(editor->GetMesh());

	preview2d[0]->show();
	preview2d[1]->show();
	preview2d[2]->show();

	SetPreviewRedraw();
}

void MainWindow::SetPreviewRedraw(void)
{
	if(preview2d[0])
		preview2d[0]->redraw = 1;
	if(preview2d[1])
		preview2d[1]->redraw = 1;
	if(preview2d[2])
		preview2d[2]->redraw = 1;
}

void MainWindow::OpenTextureMapper(void)
{
	if(tex_nr == 0)
	{
		QMessageBox::critical(this, "Texture Mapper", "No Material available");
		return;
	}

	if(editor->GetSelection()->selected_t.size() == 0)
	{
		QMessageBox::critical(this, "Texture Mapper", "No Triangles selected");
		return;
	}

	// TODO: TextureMapper soll kein Array bekommen, sondern entweder ein std::list oder gleich mit der Selection arbeiten

	CTriangle **data = new CTriangle *[editor->GetSelection()->selected_t.size()];
	set<CTriangle *>::iterator i;
	int c;
	for(c=0, i=editor->GetSelection()->selected_t.begin(); i!=editor->GetSelection()->selected_t.end(); c++, i++)
		data[c] = *i;

	texture_mapper = new TextureMapper(data, editor->GetSelection()->selected_t.size(), tex_list, tex_nr, this);
	texture_mapper->show();
}

void MainWindow::OpenMatrixDialog(void)
{
	if((editor->GetSelection()->select_mode == SELECT_MODE_VERTEX && editor->GetSelection()->selected_v.size() == 0) ||
			(editor->GetSelection()->select_mode == SELECT_MODE_TRIANGLE && editor->GetSelection()->selected_t.size() == 0))
	{
		QMessageBox::critical(this, "Matrix", "Nothing Selected");
		return;
	}

	matrix_dialog = new MatrixDialog(editor->GetSelection(), this);
	matrix_dialog->show();
}

void MainWindow::OpenEntityAttributeEditor(void)
{
	if(EntityList->currentRow() == -1 || EntityAttributeList->currentRow() == -1)
	{
		QMessageBox::critical(this, "Entity Attribute", "No Attribute selected.");
		return;
	}

	CEntity *e = editor->GetMesh()->GetEntity(EntityList->currentRow());
	string a_name = string(EntityAttributeList->currentItem()->data(Qt::UserRole).toByteArray().constData());
	CEntityAttribute *a = e->attributes.at(a_name);

	if(!a)
		return;

	entity_attribute_editor = new EntityAttributeEditor(e, a_name, a, this);
	connect(entity_attribute_editor, SIGNAL(accepted()), this, SLOT(RefreshEntityAttributeList()));
	entity_attribute_editor->show();
}

void MainWindow::RenameEntity(void)
{
	if(EntityList->currentRow() == -1)
		return;

	CEntity *e = editor->GetMesh()->GetEntity(EntityList->currentRow());
	bool ok;
	QString n = QInputDialog::getText(this, "Rename Entity", "Name:", QLineEdit::Normal, QString::fromStdString(e->name), &ok);

	if(ok)
		e->name = n.toStdString();

	RefreshEntityList();
}

void MainWindow::SetEntityGroup(void)
{
	if(EntityList->currentRow() == -1)
		return;

	CEntity *e = editor->GetMesh()->GetEntity(EntityList->currentRow());
	bool ok;
	QString n = QInputDialog::getText(this, "Set Entity Group", "Group:", QLineEdit::Normal, QString::fromStdString(e->group), &ok);

	if(ok)
		e->group = n.toStdString();

	RefreshEntityList();
}

void MainWindow::NewEntity(void)
{
	bool ok;

	QString name = QInputDialog::getText(this, "Create Entity", "Name:", QLineEdit::Normal, tr(""), &ok);

	if(!ok)
		return;

	editor->GetMesh()->CreateEntity(name.toStdString());
	RefreshEntityList();
}


void MainWindow::NewEntityAttribute(void)
{
	if(EntityList->currentRow() == -1)
	{
		QMessageBox::critical(this, "Entity Attribute", "No Entity selected.");
		return;
	}

	CEntity *e = editor->GetMesh()->GetEntity(EntityList->currentRow());

	entity_attribute_editor = new EntityAttributeEditor(e, this);
	connect(entity_attribute_editor, SIGNAL(accepted()), this, SLOT(RefreshEntityAttributeList()));
	entity_attribute_editor->show();
}

void MainWindow::DeleteEntity(void)
{
	if(EntityList->currentRow() == -1)
		return;

	CEntity *e = editor->GetMesh()->GetEntity(EntityList->currentRow());

	EntityList->clear();

	editor->GetMesh()->RemoveEntity(e);
	delete e;

	RefreshEntityList();
}

void MainWindow::DeleteEntityAttribute(void)
{
	if(EntityList->currentRow() == -1 || EntityAttributeList->currentRow() == -1)
		return;

	CEntity *e = editor->GetMesh()->GetEntity(EntityList->currentRow());
	string a_name = string(EntityAttributeList->currentItem()->data(Qt::UserRole).toByteArray().constData());
	CEntityAttribute *a = e->attributes.at(a_name);

	e->RemoveAttribute(a_name);
	delete a;

	RefreshEntityAttributeList();
}


void MainWindow::SelectButtonC(void)
{
	edit_mode = EDIT_MODE_SELECT;
	k_edit_mode = EDIT_MODE_NONE;
}

void MainWindow::MoveButtonC(void)
{
	edit_mode = EDIT_MODE_MOVE;
	k_edit_mode = EDIT_MODE_NONE;
}

void MainWindow::ScaleButtonC(void)
{
	edit_mode = EDIT_MODE_SCALE;
	k_edit_mode = EDIT_MODE_NONE;
}

void MainWindow::SelectList(void)
{
	return;
	int i;

	apply_selection = 0;

	for(i=0; i<TriangleList->count(); i++)
		TriangleList->item(i)->setSelected(false);

	apply_selection = 1;
}

void MainWindow::DeleteTriangle(void)
{
	int deletenr;
	int i;

	if(TriangleList->selectedItems().count() == 0)
		return;

	if(!QMessageBox::question(this, "Delete Triangle", "Are you sure to delete the selected Triangle(s)?", QMessageBox::Yes | QMessageBox::No))
		return;

	deletenr = TriangleList->selectedItems().count();
	for(i = 0; i < deletenr; i++)
	{
		delete tri_list[TriangleList->row(TriangleList->selectedItems().value(i))];
	}

	RefreshLists();
}

void MainWindow::RefreshLists(void)
{
	RefreshMeshLists(editor->GetMesh(), TriangleList, TextureList, tri_nr, tex_nr, tri_list, tex_list, TTexName, edit_triangle, edit_material); SetPreviewRedraw();
	RefreshPositionList();
	RefreshAnimationList();
	RefreshEntityList();
}

void MainWindow::RefreshEntityList(void)
{
	int i;
	CEntity *e;

	EntityList->clear();

	for(i=0; i<editor->GetMesh()->GetEntityCount(); i++)
	{
		e = editor->GetMesh()->GetEntity(i);
		if(e->group.length() == 0)
			EntityList->addItem(QString::fromStdString(e->name));
		else
			EntityList->addItem(QString::fromStdString(e->name) + tr(" (") + QString::fromStdString(e->group) + tr(")"));
	}

	RefreshEntityAttributeList();
}

void MainWindow::RefreshEntityAttributeList(void)
{
	map<string, CEntityAttribute *>::iterator i;
	CEntity *e;
	QListWidgetItem *item;

	EntityAttributeList->clear();

	if(EntityList->currentRow() == -1)
	{
		EntityAttributeList->setEnabled(false);
	}
	else
	{
		EntityAttributeList->setEnabled(true);
		e = editor->GetMesh()->GetEntity(EntityList->currentRow());
		for(i=e->attributes.begin(); i!=e->attributes.end(); i++)
		{
			item = new QListWidgetItem(QString(i->first.c_str()) + tr(" (") + GetEntityAttributeTypeString(i->second->type) + tr(")"));
			item->setData(Qt::UserRole, QVariant(i->first.c_str()));
			EntityAttributeList->addItem(item);
		}
	}

}

void MainWindow::RefreshMode(void)
{
	bool t_selection;
	bool m_selection;

	SeperateTrianglesAction->setEnabled(editor->GetSelection()->select_mode != SELECT_MODE_VERTEX);

	t_selection = TriangleList->selectedItems().count() == 1;
	m_selection = TextureList->selectedItems().count() == 1;

	switch(mode)
	{
	case MODE_NONE:
		TriangleWidget->setEnabled(true);
		TextureWidget->setEnabled(true);
		EditTWidget->setEnabled(false);
		EditMWidget->setEnabled(false);

		EditTButton->setEnabled(t_selection);
		EditTButton->setChecked(false);
		NewTButton->setEnabled(true);
		CloneTButton->setEnabled(false);
		DeleteTButton->setEnabled(TriangleList->selectedItems().count() >= 1);

		EditMButton->setEnabled(m_selection);
		NewMButton->setEnabled(true);
		DeleteMButton->setEnabled(selected_m_nr >= 1);

		edit_triangle = 0;
		edit_material = 0;

		gl_win->preview = 0;

		break;
	case MODE_EDIT_TRIANGLE:
		TriangleWidget->setEnabled(true);
		TextureWidget->setEnabled(false);
		EditTWidget->setEnabled(true);
		EditMWidget->setEnabled(false);

		EditTButton->setEnabled(true);
		NewTButton->setEnabled(false);
		CloneTButton->setEnabled(t_selection);
		DeleteTButton->setEnabled(false);

		EditMButton->setEnabled(false);
		NewMButton->setEnabled(false);
		DeleteMButton->setEnabled(false);

		PreviewTriangle();
		break;
	case MODE_NEW_TRIANGLE:
		TriangleWidget->setEnabled(true);
		TextureWidget->setEnabled(false);
		EditTWidget->setEnabled(true);
		EditMWidget->setEnabled(false);

		EditTButton->setEnabled(false);
		EditTButton->setChecked(false);
		NewTButton->setEnabled(true);
		CloneTButton->setEnabled(true);
		DeleteTButton->setEnabled(false);

		EditMButton->setEnabled(false);
		NewMButton->setEnabled(false);
		DeleteMButton->setEnabled(false);

		PreviewTriangle();
		break;

	case MODE_NEW_MATERIAL:
		TriangleWidget->setEnabled(false);
		TextureWidget->setEnabled(true);
		EditTWidget->setEnabled(false);
		EditMWidget->setEnabled(true);

		EditTButton->setEnabled(false);
		EditTButton->setChecked(false);
		NewTButton->setEnabled(false);
		CloneTButton->setEnabled(false);
		DeleteTButton->setEnabled(false);

		EditMButton->setEnabled(false);
		NewMButton->setEnabled(true);
		DeleteMButton->setEnabled(false);
		break;

	case MODE_EDIT_MATERIAL:
		TriangleWidget->setEnabled(false);
		TextureWidget->setEnabled(true);
		EditTWidget->setEnabled(false);
		EditMWidget->setEnabled(true);

		EditTButton->setEnabled(false);
		EditTButton->setChecked(false);
		NewTButton->setEnabled(false);
		CloneTButton->setEnabled(false);
		DeleteTButton->setEnabled(false);

		EditMButton->setEnabled(true);
		NewMButton->setEnabled(false);
		DeleteMButton->setEnabled(false);
		break;
	}

	SetPreviewRedraw();
}

void MainWindow::SelectionChange(void)
{
	if(!apply_selection)
		return;

	//CTriangle **selected_t;
	CMaterial **selected_m;
	//int selected_t_nr;
	int selected_m_nr, i;

	RefreshMode();

	/*selected_t = new CTriangle*[TriangleList->selectedItems().count()];
	selected_t_nr = TriangleList->selectedItems().count();

	for(i=0; i<selected_t_nr; i++)
	{
		selected_t[i] = tri_list[TriangleList->row(TriangleList->selectedItems().value(i))];
	}*/

	selected_m = new CMaterial*[TextureList->selectedItems().count()];
	selected_m_nr = TextureList->selectedItems().count();

	for(i=0; i<selected_m_nr; i++)
	{
		selected_m[i] = tex_list[TextureList->row(TextureList->selectedItems().value(i))];
	}

	this->selected_m = selected_m;
	this->selected_m_nr = selected_m_nr;

	//old_selection = editor->GetSelection()->selected_t = selected_t;
	//editor->GetSelection()->selected_t_nr = selected_t_nr;

	SetPreviewRedraw();
}

void MainWindow::PreviewTriangle(void)
{
	if(!gl_win->preview_t)
		gl_win->preview_t = new CTriangle(0);

	gl_win->preview_t->Set(editor->GetMesh()->CreateVertex(Vec(X1->value(), Y1->value(), Z1->value())),
				  editor->GetMesh()->CreateVertex(Vec(X2->value(), Y2->value(), Z2->value())),
				  editor->GetMesh()->CreateVertex(Vec(X3->value(), Y3->value(), Z3->value())),
								  Vec(1.0, 1.0, 1.0),
								  Vec(TX1->value(), TY1->value(), TZ1->value()),
								  Vec(TX2->value(), TY2->value(), TZ2->value()),
								  Vec(TX3->value(), TY3->value(), TZ3->value()));

	gl_win->preview = 1;
}

void MainWindow::EditTButtonC(bool checked)
{
	if(!checked)
	{
		mode = MODE_NONE;
		ResetT();
		RefreshMode();
		RefreshLists();
	}
	else
	{
		if(TriangleList->selectedItems().count() != 1)
		{
			if(TriangleList->selectedItems().count() > 1)
				QMessageBox::warning(this, "Edit Triangle", "Please Select only one item to edit!");
			EditTButton->setChecked(false);
			return;
		}

		mode = MODE_EDIT_TRIANGLE;

		edit_triangle = tri_list[TriangleList->row(TriangleList->selectedItems().value(0))];

		RefreshMode();
		RefreshLists();

		CloneT(edit_triangle);
	}
}

void MainWindow::NewTButtonC(bool checked)
{
	ResetT();
	if(!checked)
	{
		mode = MODE_NONE;
	}
	else
	{
		mode = MODE_NEW_TRIANGLE;
	}
	RefreshMode();
}

void MainWindow::CloneTButtonC(void)
{
	if((mode != MODE_EDIT_TRIANGLE && mode != MODE_NEW_TRIANGLE) || TriangleList->selectedItems().count() == 0)
		return;

	if(TriangleList->selectedItems().count() > 1)
	{
		QMessageBox::warning(this, "Clone Triangle", "Please select only one item to clone!");
		return;
	}

	CloneT(tri_list[TriangleList->row(TriangleList->selectedItems().value(0))]);
}

void MainWindow::EditMButtonC(bool checked)
{
	if(!checked)
	{
		mode = MODE_NONE;
		ResetM();
	RefreshMode();
		RefreshLists();
	}
	else
	{
		if(TextureList->selectedItems().count() != 1)
		{
			if(TextureList->selectedItems().count() > 1)
				QMessageBox::warning(this, "Edit Material", "Please Select only one item to edit!");
			EditMButton->setChecked(false);
			return;
		}

		mode = MODE_EDIT_MATERIAL;

		edit_material = tex_list[TextureList->row(TextureList->selectedItems().value(0))];

		RefreshMode();
		RefreshLists();

		CloneM(edit_material);
	}
}

void MainWindow::NewMButtonC(bool checked)
{
	ResetM();
	if(!checked)
	{
		mode = MODE_NONE;
		ResetM();
		RefreshMode();
		RefreshLists();
	}
	else
	{
		mode = MODE_NEW_MATERIAL;
	}

	RefreshMode();
}

void MainWindow::CloneM(CMaterial *m)
{
	MTexName->setText(QString::fromLocal8Bit(m->name));
	//if(m->diffuse.filename)
		Diffuse->setText(QString::fromStdString(m->diffuse.filename));
	//else
	//	Diffuse->setText("");

	//if(m->specular.filename)
		Specular->setText(QString::fromStdString(m->specular.filename));
	//else
	//	Specular->setText("");

	Exponent->setValue(m->specular.exponent);

	//if(m->normal.filename)
		Normal->setText(QString::fromStdString(m->normal.filename));
	//else
	//	Normal->setText(QString::fromLocal8Bit(""));

	//if(m->aeb.filename)
		AEB->setText(QString::fromStdString(m->aeb.filename));
	//else
	//	AEB->setText(QString::fromLocal8Bit(""));

	BumpFactor->setValue(m->aeb.bump_factor);

	//if(m->height.filename)
		Height->setText(QString::fromStdString(m->height.filename));
	//else
	//	Height->setText(QString::fromLocal8Bit(""));

	HeightFactor->setValue(m->height.factor);
}

void MainWindow::SetMaterial(void)
{
	set<CTriangle *>::iterator i;

	if(selected_m_nr == 0)
	{
		QMessageBox::critical(this, "Set Material", "No Material selected");
		return;
	}

	if(editor->GetSelection()->select_mode != SELECT_MODE_GROUP && editor->GetSelection()->select_mode != SELECT_MODE_TRIANGLE)
	{
		QMessageBox::critical(this, "Set Material", "Not in Triangle Select mode");
		return;
	}

	for(i=editor->GetSelection()->selected_t.begin(); i!=editor->GetSelection()->selected_t.end(); i++)
	{
		(*i)->m_name = cstr(selected_m[0]->name);
		(*i)->mat = selected_m[0];
	}
}

void MainWindow::CloneT(CTriangle *t)
{
	char *m_name;

	if(!t)
		return;

	X1->setValue(t->v[0]->x);
	Y1->setValue(t->v[0]->y);
	Z1->setValue(t->v[0]->z);
	X2->setValue(t->v[1]->x);
	Y2->setValue(t->v[1]->y);
	Z2->setValue(t->v[1]->z);
	X3->setValue(t->v[2]->x);
	Y3->setValue(t->v[2]->y);
	Z3->setValue(t->v[2]->z);

	R->setValue(t->color.x);
	G->setValue(t->color.y);
	B->setValue(t->color.z);

	TX1->setValue(t->tex_coord[0].x);
	TY1->setValue(t->tex_coord[0].y);
	TZ1->setValue(t->tex_coord[0].z);
	TX2->setValue(t->tex_coord[1].x);
	TY2->setValue(t->tex_coord[1].y);
	TZ2->setValue(t->tex_coord[1].z);
	TX3->setValue(t->tex_coord[2].x);
	TY3->setValue(t->tex_coord[2].y);
	TZ3->setValue(t->tex_coord[2].z);

	m_name = new char[100];
	if(t->MaterialState())
		strcpy(m_name, t->m_name);
	else
		strcpy(m_name, "$NONE");

	TTexName->setEditText(m_name);

	RefreshMode();
}

void MainWindow::ResetT(void)
{
	X1->setValue(0.0);
	Y1->setValue(0.0);
	Z1->setValue(0.0);
	X2->setValue(0.0);
	Y2->setValue(0.0);
	Z2->setValue(0.0);
	X3->setValue(0.0);
	Y3->setValue(0.0);
	Z3->setValue(0.0);

	R->setValue(1.0);
	R->setValue(1.0);
	R->setValue(1.0);

	TX1->setValue(0.0);
	TY1->setValue(0.0);
	TZ1->setValue(0.0);
	TX2->setValue(0.0);
	TY2->setValue(0.0);
	TZ2->setValue(0.0);
	TX3->setValue(0.0);
	TY3->setValue(0.0);
	TZ3->setValue(0.0);

	TTexName->lineEdit()->setText("$NONE");
}

void MainWindow::ResetM(void)
{
	MTexName->setText("");
	Diffuse->setText("");
	Specular->setText("");
	Exponent->setValue(0.0);
	Normal->setText("");
	AEB->setText("");
	Height->setText("");
}
