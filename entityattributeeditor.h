#ifndef _ENTITYATTRIBUTEEDITOR_H
#define _ENTITYATTRIBUTEEDITOR_H

#include <QDialog>

#include "ui_entityattributeeditor.h"

#include "allinc.h"

class EntityAttributeEditor : public QDialog, Ui::EntityAttributeEditor
{
	Q_OBJECT

	public:
		EntityAttributeEditor(CEntity *e, string name, CEntityAttribute *a, QWidget *parent); // to edit attributes
		EntityAttributeEditor(CEntity *e, QWidget *parent); // to create new attributes
		~EntityAttributeEditor();

	public slots:
		void Accepted(void);
		void Rejected(void);
		void ChangeType(void);

	private:
		QWidget *parent;
		CEntity *entity;
		CEntityAttribute *attribute;
		bool create;

		CEntityAttribute::Type type;
		string name;
		string original_name;
		union
		{
			CVector vec_v;
			CVector2 vec2_v;
			float float_v;
			int int_v;
		};
		string string_v;

		void Init(CEntity *e, QWidget *parent);

		void RefreshUi(void);
		void CopyContentToUi(void);
		void CopyContentFromUi(void);
		void CopyContentToAttribute(void);

	protected:
		void closeEvent(QCloseEvent *event);
};

#endif // _ENTITYATTRIBUTEEDITOR_H
