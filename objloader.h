#ifndef _OBJLOADER_H
#define _OBJLOADER_H

bool LoadMeshFromFile_libobj(const char *file, CMesh *mesh);
bool MergeWithFile_libobj(const char *file, CMesh *mesh);

#endif
