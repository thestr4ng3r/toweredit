#include "allinc.h"

Preview2D::Preview2D(void)
{
	modes = 0;
	view = 0;
	mesh = 0;
	g_scene = 0;
	pixmap = 0;
	redraw = 1;
	selection_changed = 1;

	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	this->setStyleSheet("border-style: none;");

	focus.Set(0.0, 0.0);

	mouse_right_pressed = mouse_left_pressed = 0;
}

Preview2D::Preview2D(QComboBox *c, Selection *selection, MainWindow *main_win)
{
	Preview2D();

	Create(c, selection, main_win);

	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	this->setStyleSheet("border-style: none;");

	focus.Set(0.0, 0.0);

	mouse_right_pressed = mouse_left_pressed = 0;
}

Preview2D::~Preview2D(void)
{
	if(g_scene)
		delete g_scene;
}

void Preview2D::Create(QComboBox *c, Selection *selection, MainWindow *main_win)
{
	modes = c;
	view = 0;
	redraw = 1;

	connect(modes, SIGNAL(currentIndexChanged(int)), this, SLOT(RefreshView(int)));

	g_scene = new QGraphicsScene(this);
	g_scene->setBackgroundBrush(QColor(255, 255, 255));
	item = new QGraphicsPixmapItem();
	g_scene->addItem(item);
	setScene(g_scene);
	painter = new QPainter();

	this->main_win = main_win;
	edit_mode = &main_win->real_edit_mode;

	zoom_factor = 50.0;

	public_selection = selection;

	RefreshView();
}

void Preview2D::resizeEvent(QResizeEvent *event)
{
	QGraphicsView::resizeEvent(event);
	redraw = 1;
}

void Preview2D::mousePressEvent(QMouseEvent *event)
{
	int i, old_select_mode;

	QGraphicsView::mousePressEvent(event);

	main_win->SetPreviewRedraw();

	if(event->buttons() & Qt::RightButton && !mouse_left_pressed)
	{
		mouse_right_pressed = 1;

		old_mouse_x = event->globalX();
		old_mouse_y = event->globalY();

		QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
		setCursor(QCursor(Qt::BlankCursor));
	}

	if(event->buttons() & Qt::LeftButton && !mouse_right_pressed)
	{
		edit_action = *edit_mode;
		mouse_left_pressed = 1;
		switch(*edit_mode)
		{
			case EDIT_MODE_SELECT:
				select_point[1] = select_point[0] = ReTransform(Vec(event->x(), event->y()));
				break;
			case EDIT_MODE_MOVE:
				QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
				setCursor(QCursor(Qt::BlankCursor));
				old_mouse_x = event->globalX();
				old_mouse_y = event->globalY();
			case EDIT_MODE_SCALE:
			old_select_mode = public_selection->select_mode;
			public_selection->SwitchToVertexSelectMode();
			transform_v_nr = public_selection->selected_v.size();
					transform_v = new CVertex *[transform_v_nr];
			set<CVertex *>::iterator it;
			for(i=0, it=public_selection->selected_v.begin(); it!=public_selection->selected_v.end(); it++, i++) // TODO: Gleich mit der Selection arbeiten!
				transform_v[i] = *it;
			public_selection->select_mode = old_select_mode;
			/*switch(old_select_mode)
					{
						case SELECT_MODE_TRIANGLE:
					public_selection->SwitchToTriangleSelectMode();
					break;
				case SELECT_MODE_GROUP:
					public_selection->SwitchToGroupSelectMode();
					break;
					}*/
			break;
		}
		switch(*edit_mode)
		{
			case EDIT_MODE_SCALE:
				scale_origin = Vec(event->x(), event->y());
				scale_center = Vec(0.0, 0.0, 0.0);
				for(i=0; i<transform_v_nr; i++)
					scale_center += (CVector)*(transform_v[i]);
				scale_center *= 1 / (float)transform_v_nr;
				scale_center_2d = Transform(scale_center);
				scale_base = (scale_center_2d - scale_origin).Len();
				scale_factor = 1.0;
				break;
		}
	}
}


void Preview2D::mouseReleaseEvent(QMouseEvent *event)
{
	float x1, x2, y1, y2;
	int add;

	main_win->SetPreviewRedraw();

	QGraphicsView::mouseReleaseEvent(event);

	if(mouse_left_pressed)
	{
		switch(*edit_mode)
		{
		case EDIT_MODE_SELECT:
			mouse_left_pressed = 0;

			if(mesh)
			{
				if(mesh->GetState())
				{
					x1 = select_point[0].x < select_point[1].x ? select_point[0].x : select_point[1].x;
					x2 = select_point[0].x > select_point[1].x ? select_point[0].x : select_point[1].x;
					y1 = select_point[0].y < select_point[1].y ? select_point[0].y : select_point[1].y;
					y2 = select_point[0].y > select_point[1].y ? select_point[0].y : select_point[1].y;

			add = event->modifiers() & Qt::ShiftModifier ? 1 : 0;

					switch(public_selection->select_mode)
					{
					case SELECT_MODE_GROUP:
						SelectGroups(x1, y1, x2, y2);
						break;
					case SELECT_MODE_TRIANGLE:
						SelectTriangles(x1, y1, x2, y2, add);
						break;
					case SELECT_MODE_VERTEX:
						SelectVertices(x1, y1, x2, y2);
						break;
					}
					selection_changed = 1;
				}
			}
			break;
		case EDIT_MODE_MOVE:
			QCursor::setPos(old_mouse_x, old_mouse_y);
			setCursor(QCursor(Qt::ArrowCursor));
			break;
		case EDIT_MODE_SCALE:
			break;
		}
	}

	if(mouse_right_pressed)
	{
		mouse_right_pressed = 0;
		QCursor::setPos(old_mouse_x, old_mouse_y);
		setCursor(QCursor(Qt::ArrowCursor));
	}
}


void Preview2D::SelectGroups(float x1, float y1, float x2, float y2, int add) // TODO: add
{
	CTriangle **selected_t;
	int selected_t_nr;
	int found_t_nr;
	int skip;
	int i, j, k, p, ti;
	CTriangle *t;
	CVector2 v;

	selected_t = new CTriangle *[mesh->GetTriangleCount()];
	selected_t_nr = 0;

	for(ti=0; ti<mesh->GetTriangleCount(); ti++)
	{
		t = mesh->GetTriangle(ti);
		for(i=0, p=0; i<3; i++)
		{
			v = TransformToView(*t->v[i], view);

			if(v.x > x1 && v.x < x2 && v.y > y1 && v.y < y2)
				p++;
		}
		if(p>0)
		{
			selected_t[selected_t_nr] = t;
			selected_t_nr++;
		}
	}

	found_t_nr = 1;

	while(found_t_nr > 0 )
	{
		found_t_nr = 0;
		for(ti=0; ti<mesh->GetTriangleCount(); ti++)
		{
			t = mesh->GetTriangle(ti);
			skip = 0;
			for(i=0; i<selected_t_nr; i++)
				if(selected_t[i] == t)
				{
					skip = 1;
					break;
				}
			if(skip)
				continue;
			for(i=0; i<selected_t_nr; i++)
			{
				p=0;
				for(j=0; j<3; j++)
					for(k=0; k<3; k++)
					{
						if(t->v[j] == selected_t[i]->v[k])
							p++;
					}

				if(p>=2)
				{
					selected_t[selected_t_nr] = t;
					selected_t_nr++;
					found_t_nr++;
					break;
				}
			}
		}
	}

	if(!add)
		public_selection->ClearSelection();
	public_selection->AddTriangles(selected_t, selected_t_nr);
}

void Preview2D::SelectTriangles(float x1, float y1, float x2, float y2, int add)
{
	int i, p, ti;
	CTriangle *t;
	CVector2 v;

	if(!add)
		public_selection->ClearSelection();

	for(ti=0; ti<mesh->GetTriangleCount(); ti++)
	{
		t = mesh->GetTriangle(ti);
		for(i=0, p=0; i<3; i++)
		{
			v = TransformToView(*t->v[i], view);

			if(v.x > x1 && v.x < x2 && v.y > y1 && v.y < y2)
				p++;
		}
		if(p==3)
		{
			public_selection->AddTriangle(t);
		}
	}
}

void Preview2D::SelectVertices(float x1, float y1, float x2, float y2, int add) // TODO: add
{
	CVertex **selected_v;
	int selected_v_nr;
	CVertex *t;
	CVector2 v;
	int ti;

	selected_v = new CVertex *[mesh->GetVertexCount()];
	selected_v_nr = 0;

	for(ti=0; ti<mesh->GetVertexCount(); ti++)
	{
		t = mesh->GetVertex(ti);
		v = TransformToView((CVector)*t, view);

		if(v.x > x1 && v.x < x2 && v.y > y1 && v.y < y2)
		{
			selected_v[selected_v_nr] = t;
			selected_v_nr++;
		}
	}

	if(!add)
		public_selection->ClearSelection();
	public_selection->AddVertices(selected_v, selected_v_nr);
}

void Preview2D::mouseMoveEvent(QMouseEvent *event)
{
	QGraphicsView::mouseMoveEvent(event);
	CVector dist;
	CVector calc;
	int i;
	float scale_dist;

	if(mouse_left_pressed || mouse_right_pressed)
		main_win->SetPreviewRedraw();

	if(mouse_left_pressed)
	{
		switch(edit_action)
		{
		case EDIT_MODE_SELECT:
			select_point[1] = ReTransform(Vec(event->x(), event->y()));
			break;
		case EDIT_MODE_MOVE:
			dist = ReTransformVRel(Vec(event->x() - width() / 2, event->y() - height() / 2));
			for(i=0; i<transform_v_nr; i++)
			*(transform_v[i]) += dist;

			QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
			break;
		case EDIT_MODE_SCALE:
			scale_dist = (Vec(event->x(), event->y()) - scale_center_2d).Len();
			scale_dist = scale_dist / scale_base;
			for(i=0; i<transform_v_nr; i++)
			{
				calc = scale_center + ((*transform_v[i] - scale_center) * (1 / scale_factor));
				calc = scale_center + ((calc - scale_center) * scale_dist);
		transform_v[i]->Set(calc.x, calc.y, calc.z);
			}
			scale_factor = scale_dist;
			break;
		}
	}

	if(mouse_right_pressed)
	{
		focus.y += ((   event->y() - (height() / 2.0)   ) * -0.5) / zoom_factor;
		focus.x += ((   event->x() - (width() / 2.0)    ) * -0.5) / zoom_factor;
		QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
	}
}

void Preview2D::wheelEvent(QWheelEvent *event)
{
	QGraphicsView::wheelEvent(event);

	redraw = 1;

	float speed;

	if(event->modifiers() & Qt::ShiftModifier)
	   speed = 0.5;
	else if(event->modifiers() & Qt::ControlModifier)
	   speed = 2.0;
	else
	   speed = 1.0;

	speed *= 0.1;

	zoom_factor += zoom_factor * speed * ((float)event->delta() / 120.0);
}

CVector2 Preview2D::Transform(CVector2 v)
{
	return TransformToPreview(v, focus, zoom_factor, Vec(width(), height()));
}

CVector2 Preview2D::ReTransform(CVector2 v)
{
	return TransformFromPreview(v, focus, zoom_factor, Vec(width(), height()));
}

void Preview2D::SetMesh(CMesh *m)
{
	if(!m)
		return;

	mesh = m;
}


void Preview2D::RefreshView(int index)
{
	if(!modes)
		return;

	if(index == -1)
	{
		view = modes->currentIndex();
	}
	else
	{
		view = index;
	}

	redraw = 1;
}


void Preview2D::RefreshPreview(void)
{
	if(!redraw)
		return;
	redraw = 0;

	CTriangle *t;
	CVertex *vt;
	CVector2 v[3];
	CVector2 center;

	int lines;
	int i, ti, vi;
	set<CTriangle *>::iterator itt;
	set<CVertex *>::iterator itv;
	float line_dist;
	float line_pos;
	CVector2 line_v;
	CVector2 zp = Transform(Vec(0.0, 0.0));

	float view_width, view_height;

	static QPen normal_pen(QColor(0, 0, 0));
	static QPen selected_pen(QColor(255, 0, 0));
	static QPen line_pen(QColor(150, 150, 150));
	static QPen axes_3d_x_pen(QColor(255, 0.0, 0.0));
	static QPen axes_3d_y_pen(QColor(255, 255, 0.0));
	static QPen axes_3d_z_pen(QColor(0.0, 0.0, 255));
	static QPen select_pen(QColor(100, 100, 100));
	static QPen selected_vertex_pen(QColor(255, 0, 0));
	static QPen vertex_pen(QColor(0, 0, 0));
	static QPen text_pen(QColor(0, 0, 0));

	axes_3d_x_pen.setWidth(2);
	axes_3d_y_pen.setWidth(2);
	axes_3d_z_pen.setWidth(2);
	vertex_pen.setWidth(4);
	selected_vertex_pen.setWidth(4);

	int max_width, max_height;

	max_width = width();
	max_height = height();
	center.Set(max_width / 2, max_height / 2);

	g_scene->setSceneRect(0, 0, max_width, max_height);

	view_width = max_width / zoom_factor;
	view_height = max_height / zoom_factor;

	line_dist = ceil(view_width / 10.0);

	pixmap = QPixmap(max_width, max_height);
	pixmap.fill(QColor(255, 255, 255));

	if(!painter->begin(&pixmap))
		return;

	if(line_dist)
	{
		painter->setPen(line_pen);

		lines = ceil((view_width / line_dist) / 2);
		for(i = -lines; i <= lines; i += 1)
		{
			line_pos = (focus.x -fmod(focus.x, line_dist)) + i * line_dist;
			line_v = Transform(Vec(line_pos, 0.0));
			painter->drawLine(line_v.x, 0.0, line_v.x, max_height);
		}

		lines = ceil(ceil(view_height) / (2 * line_dist));
		for(i = -lines; i <= lines; i += 1)
		{
			line_pos = (focus.y - fmod(focus.y, line_dist)) + i * line_dist;
			line_v = Transform(Vec(0.0, line_pos));
			painter->drawLine(0.0, line_v.y, max_width, line_v.y);
		}

	painter->setPen(text_pen);
	painter->drawText(3, 13, QString::number((double)line_dist));
	}

	switch(view)
	{
	case PREVIEW_VIEW_FRONT:
		painter->setPen(axes_3d_x_pen);
		painter->drawLine(zp.x, zp.y, Transform(Vec(1.0, 0.0)).x, zp.y);
		painter->setPen(axes_3d_y_pen);
		painter->drawLine(zp.x, zp.y, zp.x, Transform(Vec(0.0, -1.0)).y);
		break;
	case PREVIEW_VIEW_TOP:
		painter->setPen(axes_3d_x_pen);
		painter->drawLine(zp.x, zp.y, Transform(Vec(1.0, 0.0)).x, zp.y);
		painter->setPen(axes_3d_z_pen);
		painter->drawLine(zp.x, zp.y, zp.x, Transform(Vec(0.0, 1.0)).y);
		break;
	case PREVIEW_VIEW_LEFT:
		painter->setPen(axes_3d_z_pen);
		painter->drawLine(zp.x, zp.y, Transform(Vec(1.0, 0.0)).x, zp.y);
		painter->setPen(axes_3d_y_pen);
		painter->drawLine(zp.x, zp.y, zp.x, Transform(Vec(0.0, -1.0)).y);
		break;
	case PREVIEW_VIEW_BACK:
		painter->setPen(axes_3d_x_pen);
		painter->drawLine(zp.x, zp.y, Transform(Vec(-1.0, 0.0)).x, zp.y);
		painter->setPen(axes_3d_y_pen);
		painter->drawLine(zp.x, zp.y, zp.x, Transform(Vec(0.0, -1.0)).y);
		break;
	case PREVIEW_VIEW_BOTTOM:
		painter->setPen(axes_3d_x_pen);
		painter->drawLine(zp.x, zp.y, Transform(Vec(-1.0, 0.0)).x, zp.y);
		painter->setPen(axes_3d_z_pen);
		painter->drawLine(zp.x, zp.y, zp.x, Transform(Vec(0.0, 1.0)).y);
		break;
	case PREVIEW_VIEW_RIGHT:
		painter->setPen(axes_3d_z_pen);
		painter->drawLine(zp.x, zp.y, Transform(Vec(-1.0, 0.0)).x, zp.y);
		painter->setPen(axes_3d_y_pen);
		painter->drawLine(zp.x, zp.y, zp.x, Transform(Vec(0.0, -1.0)).y);
		break;
	}

	if(mesh)
	{
		if(mesh->GetState())
		{
			for(ti=0; ti<mesh->GetTriangleCount(); ti++)
			{
				t = mesh->GetTriangle(ti);
				painter->setPen(normal_pen);
				if(public_selection->select_mode == SELECT_MODE_TRIANGLE)
				{
					if(public_selection->selected_t.find(t) != public_selection->selected_t.end())
						continue;
				}

				v[0] = Transform(*t->v[0]);
				v[1] = Transform(*t->v[1]);
				v[2] = Transform(*t->v[2]);

				painter->drawLine(v[0].x, v[0].y, v[1].x, v[1].y);
				painter->drawLine(v[1].x, v[1].y, v[2].x, v[2].y);
				painter->drawLine(v[2].x, v[2].y, v[0].x, v[0].y);
			}
			if(public_selection->select_mode == SELECT_MODE_VERTEX)
			{
				painter->setPen(vertex_pen);
				for(vi=0; vi<mesh->GetVertexCount(); vi++)
				{
					vt = mesh->GetVertex(vi);
					v[0] = Transform(*vt);
					painter->drawPoint(v[0].x, v[0].y);
				}
			}

			switch(public_selection->select_mode)
			{
			case SELECT_MODE_TRIANGLE:
			case SELECT_MODE_GROUP:
				painter->setPen(selected_pen);
				for(itt=public_selection->selected_t.begin(); itt!=public_selection->selected_t.end(); itt++)
				{
					v[0] = Transform(*(*itt)->v[0]);
					v[1] = Transform(*(*itt)->v[1]);
					v[2] = Transform(*(*itt)->v[2]);

					painter->drawLine(v[0].x, v[0].y, v[1].x, v[1].y);
					painter->drawLine(v[1].x, v[1].y, v[2].x, v[2].y);
					painter->drawLine(v[2].x, v[2].y, v[0].x, v[0].y);
				}
				break;
			case SELECT_MODE_VERTEX:
				painter->setPen(selected_vertex_pen);
				for(itv=public_selection->selected_v.begin(); itv!=public_selection->selected_v.end(); itv++)
				{
					v[0] = Transform(**itv);
					painter->drawPoint(v[0].x, v[0].y);
				}
			}
		}
	}

	if(mouse_left_pressed && edit_action == EDIT_MODE_SELECT)
	{
		CVector2 select[2] = { Transform(select_point[0]), Transform(select_point[1]) - Transform(select_point[0]) };
		painter->setPen(select_pen);
		painter->drawRect(select[0].x, select[0].y, select[1].x, select[1].y);
	}

	painter->end();

	item->setPixmap(pixmap);
}





































