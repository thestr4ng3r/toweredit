#include "allinc.h"

NormalsCalculator::NormalsCalculator(Editor *editor, QWidget *parent) : QDialog(parent), Ui::NormalsCalculator()
{
	this->parent = parent;
	this->editor = editor;

	setupUi(this);

	connect(Buttons, SIGNAL(clicked(QAbstractButton *)), this, SLOT(Click(QAbstractButton *)));
}


NormalsCalculator::~NormalsCalculator()
{
}

void NormalsCalculator::showEvent(QShowEvent *event)
{
	QDialog::showEvent(event);
	parent->setEnabled(false);
	setEnabled(true);
}


void NormalsCalculator::closeEvent(QCloseEvent *event)
{
	QDialog::closeEvent(event);
	parent->setEnabled(true);
}


void NormalsCalculator::Click(QAbstractButton *b)
{
	switch(Buttons->standardButton(b))
	{
		case QDialogButtonBox::Ok:
			Ok();
			break;
		case QDialogButtonBox::Cancel:
			Cancel();
			break;
		default:
			break;
	}
}

void NormalsCalculator::Ok(void)
{
	if(SolidRadio->isChecked())
		editor->CalculateNormalsSolid();
	else if(SmoothRadio->isChecked())
		editor->CalculateNormalsSmooth();
	close();
}


void NormalsCalculator::Cancel(void)
{
}
