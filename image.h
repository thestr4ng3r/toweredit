#ifndef _IMAGE_H
#define _IMAGE_H

#include <QPixmap>

QPixmap LoadTGAImage(const char *filename);

#endif // IMAGE_H
