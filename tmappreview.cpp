#include "allinc.h"

TMapPreview::TMapPreview(QComboBox *mode, CTriangle ***t, int tn, CVector2***c, CMaterial **m)
{
	modes = mode;
	triangles = t;
	triangle_nr = tn;
	coords = c;
	material = m;
	loaded_material = 0;
	scale = 512.0;
	offset = Vec(0.0, 0.0);
	mouse_left_pressed = mouse_right_pressed = 0;

	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	setStyleSheet("border-style: none;");

	min.Set(0.0, 0.0);
	max.Set(1.0, 1.0);

	g_scene = new QGraphicsScene(this);
	g_scene->setBackgroundBrush(QColor(0, 0, 0));
	item = new QGraphicsPixmapItem();
	g_scene->addItem(item);
	setScene(g_scene);
	painter = new QPainter();
}

TMapPreview::~TMapPreview(void)
{
}

void TMapPreview::RefreshPreview(void)
{
	int i, v;
	QPixmap scaled_material;
	QImage *image;

	if(loaded_material != *material)
	{
	loaded_material = *material;
	image = new QImage();
	image->load((*material)->diffuse.filename.c_str()); // TODO: Load with path!!!
	material_pixmap = QPixmap::fromImage(*image);
	delete image;
	if(material_pixmap.isNull())
		material_pixmap = LoadTGAImage((*material)->diffuse.filename.c_str());
	if(material_pixmap.isNull())
	{
		image = new QImage(128, 128, QImage::Format_RGB32);
		image->setColor(0, Qt::gray);
		image->fill(0);
		material_pixmap = QPixmap::fromImage(*image);
		delete image;
	}
	}

	scaled_material = material_pixmap.scaled((int)scale, (int)scale);

	QPen point_pen(QColor(255, 0, 0));
	point_pen.setWidth(5);
	QPen line_pen(QColor(200, 200, 200));

	pixmap = QPixmap(width(), height());

	painter->begin(&pixmap);
	painter->fillRect(pixmap.rect(), Qt::gray);
	painter->drawPixmap(offset.x * scale, offset.y * scale, scaled_material.width(), scaled_material.height(), scaled_material);

	for(i=0; i<triangle_nr; i++)
	{
		for(v=0; v<3; v++)
		{
			painter->setPen(line_pen);
		painter->drawLine(offset.x * scale + (*triangles)[i]->tex_coord[v].x * scaled_material.width(),
				  offset.y * scale + (*triangles)[i]->tex_coord[v].y * scaled_material.height(),
				  offset.x * scale + (*triangles)[i]->tex_coord[v < 2 ? v + 1 : 0].x * scaled_material.width(),
				  offset.y * scale + (*triangles)[i]->tex_coord[v < 2 ? v + 1 : 0].y * scaled_material.height());
		}
	}

	for(i=0; i<triangle_nr; i++)
	{
		for(v=0; v<3; v++)
		{
			painter->setPen(point_pen);
		painter->drawPoint(offset.x * scale + (*triangles)[i]->tex_coord[v].x * scaled_material.width(),
				   offset.y * scale + (*triangles)[i]->tex_coord[v].y * scaled_material.height());
		}
	}

	painter->end();
	item->setPixmap(pixmap);

	g_scene->setSceneRect(0, 0, width(), height());
}

void TMapPreview::wheelEvent(QWheelEvent *event)
{
	float new_scale, delta;

	new_scale = scale * pow(1.25, (double)event->delta() / 120.0); // if delta / 120 == -1 (for example), scale is multiplied by 2.0^-1.0 = 0.5
	delta = 1.0 / new_scale - 1.0 / scale;
	offset.x += delta;
	offset.y += delta;
	scale = new_scale;
}

void TMapPreview::mousePressEvent(QMouseEvent *event)
{
	if(event->buttons() & Qt::RightButton && !mouse_left_pressed)
	{
		mouse_right_pressed = 1;

		old_mouse_x = event->globalX();
		old_mouse_y = event->globalY();

		QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
		setCursor(QCursor(Qt::BlankCursor));
	}
}

void TMapPreview::mouseReleaseEvent(QMouseEvent *event)
{
	QGraphicsView::mouseReleaseEvent(event);
	if(mouse_right_pressed)
	{
		mouse_right_pressed = 0;
		QCursor::setPos(old_mouse_x, old_mouse_y);
		setCursor(QCursor(Qt::ArrowCursor));
	}
}

void TMapPreview::mouseMoveEvent(QMouseEvent *event)
{
	if(mouse_right_pressed)
	{
		offset.y += ((   event->y() - (height() / 2.0)   ) * 0.5) / scale;
		offset.x += ((   event->x() - (width() / 2.0)    ) * 0.5) / scale;
		QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
	}
}










