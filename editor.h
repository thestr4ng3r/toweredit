#ifndef _EDITOR_H
#define _EDITOR_H

#include "allinc.h"
class Editor : public QApplication
{
public:
	Editor(int &argc, char **argv);
	~Editor(void);

	void Init(const char *file = 0);

	MainWindow *GetMainWindow(void)			{ return main_win; }
	Selection *GetSelection(void)			{ return selection; }
	CMesh *GetMesh(void)				{ return mesh; }
	char *GetFileName(void)				{ return filename; }
	bool GetAnimationMode(void)			{ return animation_mode; }

	bool SetAnimationMode(bool animation_mode)	{ return this->animation_mode = animation_mode; }
	void SetFileName(const char *filename)		{ if(this->filename) delete [] this->filename; if(filename) this->filename = cstr(filename); else filename = 0; }

	void SetBusy(bool busy)				{ this->busy = busy; }
	void SetProgress(bool enabled,
			 float progress = 0.0)		{ this->progress_enabled = enabled; this->progress = progress; }
	void SetStatusMessage(bool enabled,
				  const char *message)	{ status_message_enabled = enabled; if(enabled) status_message = QString::fromLocal8Bit(message); printf("%s\n", message); }

	bool GetBusy(void)				{ return busy; }
	bool GetProgressEnabled(void)			{ return progress_enabled; }
	float GetProgress(void)				{ return progress; }
	bool GetStatusMessageEnabled(void)		{ return status_message_enabled; }
	QString GetStatusMessage(void)			{ return status_message; }

	void RefreshUi(void);

	bool Load(const char *file);

	bool LoadFromFile_libobj(const char *file, int merge = 0);

	void CalculateNormalsSolid(void);
	void CalculateNormalsSmooth(void);

	struct preview
	{
		bool axes;
		bool wireframe;
		bool normals;
		bool both_sides;
	} preview;

	virtual bool notify(QObject * receiver, QEvent * event)
	{
		try
		{
			return QApplication::notify(receiver, event);
		}
		catch(std::exception& e)
		{
			qCritical() << "Exception thrown:" << e.what();
		}
		return false;
	}

private:
	MainWindow *main_win;
	Selection *selection;
	CMesh *mesh;

	char *filename;

	bool animation_mode;

	bool busy;
	float progress;
	bool progress_enabled;
	QString status_message;
	bool status_message_enabled;
};

QString GetEntityAttributeTypeString(CEntityAttribute::Type t);


void RefreshApp(void);

#endif // CEDITOR_H
