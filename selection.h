#ifndef _SELECTION_H
#define _SELECTION_H

struct Selection
{
	Editor *editor;

	int select_mode;

	set<CTriangle *> selected_t;
	set<CVertex *> selected_v;
	
	Selection(Editor *editor);
	
	void SwitchToVertexSelectMode(void);
	void SwitchToTriangleSelectMode(void);
	void SwitchToGroupSelectMode(void);

	void ConvertToVertices(void);
	void ConvertToTriangles(void);
	
	void AddTriangles(CTriangle **tr, int nr);
	void AddVertices(CVertex **vt, int nr);

	void AddTriangle(CTriangle *t);
	void AddVertex(CVertex *v);

	bool IsTriangleSelected(CTriangle *t);
	bool IsVertexSelected(CVertex *v);
	
	void ClearSelection(void);

	void DeleteSelection(void);
};

#endif // SELECTION_H
