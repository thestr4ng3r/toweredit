#include "allinc.h"

#ifdef TMS_USE_LIB_G3D
G3DContext *CMesh::g3d_context = 0;
#endif

Editor *editor;
QApplication *app;

int main(int argc, char *argv[])
{
//	app = new QApplication(argc, argv);


	editor = new Editor(argc, argv);
	app = editor;
	setlocale(LC_NUMERIC,"C");

	if(argc > 1)
		editor->Init(argv[1]);
	else
		editor->Init();

	return app->exec();
}

#define UPDATE_INTERVAL 50

void RefreshApp(void)
{
	static long last_update = 0;

	if(QDateTime::currentMSecsSinceEpoch() - last_update > UPDATE_INTERVAL)
	{
		editor->RefreshUi();
		app->processEvents();
		last_update = QDateTime::currentMSecsSinceEpoch();
	}
}


#ifdef TMS_USE_LIB_3DS
CVector Vec3ds(Lib3dsVector v)
{
	return Vec(v[0], v[1], v[2]);
}
#endif

#ifdef TMS_USE_LIB_G3D
CVector VecG3D(G3DVector v[3])
{
	return Vec(v[0], v[1], v[2]);
}
#endif

int CMesh::LoadFromFile_libg3d(const char *file)
{
#ifdef TMS_USE_LIB_G3D
	Create();
	return MergeWithFile_libg3d(file);
#endif
	return 0;
}

int CMesh::MergeWithFile_libg3d(const char *file)
{
#ifdef TMS_USE_LIB_G3D
	G3DModel *model;
	G3DObject *o;
	GSList *ol;
	G3DFace *f;
	GSList *fl;
	CVector v[4];
	CVector uv[4];
	char no_tex[100];
	int c = 0;
	snprintf(no_tex, 100, "$NONE");

	CreateG3DContext();
	if(!(model = g3d_model_load_full(g3d_context, file, G3D_MODEL_OPTIMIZE)))
		return 0;

	for(ol=model->objects; ol; ol=ol->next)
	{
		o = (G3DObject *)ol->data;
		for(fl=o->faces; fl; fl=fl->next)
		{
			f = (G3DFace *)fl->data;
			v[0] = Vec(o->vertex_data[f->vertex_indices[0] * 3 + 0],
					   o->vertex_data[f->vertex_indices[0] * 3 + 1],
					   o->vertex_data[f->vertex_indices[0] * 3 + 2]);
			v[1] = Vec(o->vertex_data[f->vertex_indices[1] * 3 + 0],
					   o->vertex_data[f->vertex_indices[1] * 3 + 1],
					   o->vertex_data[f->vertex_indices[1] * 3 + 2]);
			v[2] = Vec(o->vertex_data[f->vertex_indices[2] * 3 + 0],
					   o->vertex_data[f->vertex_indices[2] * 3 + 1],
					   o->vertex_data[f->vertex_indices[2] * 3 + 2]);
		c++;

		if(f->tex_vertex_count >= 3)
		{
			uv[0] = Vec(f->tex_vertex_data[0 * 3 + 0],
				f->tex_vertex_data[0 * 3 + 1],
				f->tex_vertex_data[0 * 3 + 2]);
			uv[1] = Vec(f->tex_vertex_data[1 * 3 + 0],
				f->tex_vertex_data[1 * 3 + 1],
				f->tex_vertex_data[1 * 3 + 2]);
				uv[2] = Vec(f->tex_vertex_data[2 * 3 + 0],
				f->tex_vertex_data[2 * 3 + 1],
				f->tex_vertex_data[2 * 3 + 2]);
			printf("%d: %f, %f, %f -> %f, %f, %f -> %f, %f, %f\n", f->tex_vertex_count,
			   pvec(uv[0]), pvec(uv[1]), pvec(uv[2]));
		}
		else
		{
			uv[0] = uv[1] = uv[2] = Vec(0.0, 0.0, 0.0);
			printf("no uv info\n");
		}

			this->CreateTriangleAuto(v[0], v[1], v[2],
								 Vec(1.0, 1.0, 1.0), no_tex,
								 uv[0], uv[1], uv[2]);

			if(f->vertex_count == 4)
			{
				v[3] = Vec(o->vertex_data[f->vertex_indices[3] * 3 + 0],
						   o->vertex_data[f->vertex_indices[3] * 3 + 1],
						   o->vertex_data[f->vertex_indices[3] * 3 + 2]);
		if(f->tex_vertex_count == 4)
			uv[3] = Vec(f->tex_vertex_data[3 * 3 + 0],
				f->tex_vertex_data[3 * 3 + 1],
					f->tex_vertex_data[3 * 3 + 2]);
		else
			uv[3] = Vec(0.0, 0.0, 0.0);
				this->CreateTriangleAuto(v[2], v[3], v[0],
									 Vec(1.0, 1.0, 1.0), no_tex,
									 uv[2], uv[3], uv[0]);
			}
		}
	}

	printf("imported %d faces\n", c);

	g3d_model_free(model);

	return 1;
#else
	return 0;
#endif
}

int CMesh::LoadFromFile_lib3ds(const char *file)
{
#ifdef TMS_USE_LIB_3DS
	Create();
	return MergeWithFile_lib3ds(file);
#endif
	return 0;
}

int CMesh::MergeWithFile_lib3ds(const char *file)
{
#ifdef TMS_USE_LIB_3DS
	Lib3dsFile *f;
	Lib3dsMesh *m;
	Lib3dsVector *v;
	Lib3dsFace *face;
	Lib3dsTexel *texel;
	CVector normal;
	CVector normalc;
	CVector t;
	CVector vc[3];
	CVector uv[3];
	unsigned int faces, fii, fi, i;
	char no_tex[100];

	snprintf(no_tex, 100, "$NONE");

	f = lib3ds_file_load(file);
	if(!f)
		return 0;

	faces = 0;

	for(m = f->meshes; m; m = m->next)
	{
		faces += m->faces;
	}
	v = new Lib3dsVector[faces  * 3];
	fii = 0;

	for(m = f->meshes; m; m = m->next)
	{
		for(fi = 0; fi < m->faces; fi++)
		{
			face = &m->faceL[fi];
			for(i=0; i<3; i++)
			{
				memcpy(&v[fii * 3 + i], m->pointL[face->points[i]].pos, sizeof(Lib3dsVector));
		vc[i] = Vec3ds(v[fii * 3 + i]);
		if(fi+i < m->texels)
		{
			texel = &m->texelL[fi + i];
			uv[i] = Vec((*texel)[0], (*texel)[1], 0.0);
		}
			uv[i] = Vec(0.0, 0.0, 0.0);
			}
		normal = Vec3ds(face->normal);
		normal.Normalize();
		normalc = Cross(vc[1]-vc[0], vc[2]-vc[0]);
		normalc.Normalize();
		if(Dot(normal, normalc) < -0.1)
		{
			printf("%f, %f, %f should be %f, %f, %f\n", pvec(normal), pvec(normalc));
			t = vc[0];
			vc[0] = vc[2];
			vc[2] = t;
		}
			CreateTriangle(CreateVertex(vc[0]), CreateVertex(vc[1]), CreateVertex(vc[2]),
			   Vec(1.0, 1.0, 1.0), no_tex, uv[0], uv[1], uv[2]);
			fii++;
		}
	}

	if(m)
		lib3ds_mesh_free(m);
	lib3ds_file_free(f);

	return 1;
#else
	return 0;
#endif
}
