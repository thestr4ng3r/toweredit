#include "allinc.h"

Selection::Selection(Editor *editor)
{
	this->editor = editor;
	ClearSelection();
	select_mode = SELECT_MODE_TRIANGLE;
}

bool Selection::IsTriangleSelected(CTriangle *t)
{
	return selected_t.find(t) != selected_t.end();
}

bool Selection::IsVertexSelected(CVertex *v)
{
	return selected_v.find(v) != selected_v.end();
}

void Selection::AddTriangle(CTriangle *t)
{
	selected_t.insert(t);
}

void Selection::AddVertex(CVertex *v)
{
	selected_v.insert(v);
}

void Selection::ConvertToVertices(void)
{
	if(select_mode == SELECT_MODE_VERTEX)
		return;

	set<CTriangle *>::iterator i;

	selected_v.clear();
	for(i=selected_t.begin(); i!=selected_t.end(); i++)
	{
		AddVertex((*i)->v[0]);
		AddVertex((*i)->v[1]);
		AddVertex((*i)->v[2]);
	}
}

void Selection::ConvertToTriangles(void)
{
	if(select_mode == SELECT_MODE_GROUP || select_mode == SELECT_MODE_TRIANGLE)
		return;

	int i;
	CTriangle *t = 0;

	selected_t.clear();
	for(i=0; i<editor->GetMesh()->GetTriangleCount(); i++)
	{
		t = editor->GetMesh()->GetTriangle(i);

		if(selected_v.find(t->v[0]) == selected_v.end())
			continue;
		if(selected_v.find(t->v[1]) == selected_v.end())
			continue;
		if(selected_v.find(t->v[2]) == selected_v.end())
			continue;
		AddTriangle(t);
	}
}

void Selection::SwitchToGroupSelectMode(void)
{
	ConvertToTriangles();
	select_mode = SELECT_MODE_GROUP;
}

void Selection::SwitchToTriangleSelectMode(void)
{
	ConvertToTriangles();
	select_mode = SELECT_MODE_TRIANGLE;
}

void Selection::SwitchToVertexSelectMode(void)
{
	ConvertToVertices();
	select_mode = SELECT_MODE_VERTEX;
}

void Selection::AddTriangles(CTriangle **tr, int nr)
{
	int i;

	for(i=0; i<nr; i++)
		AddTriangle(tr[i]);
}

void Selection::AddVertices(CVertex **vr, int nr)
{
	int i;

	for(i=0; i<nr; i++)
		AddVertex(vr[i]);
}

void Selection::ClearSelection(void)
{
	selected_t.clear();
	selected_v.clear();
}

void Selection::DeleteSelection(void)
{
	set<CTriangle *>::iterator t;
	set<CVertex *>::iterator v;

	switch(select_mode)
	{
		case SELECT_MODE_VERTEX:
			for(v=selected_v.begin(); v!=selected_v.end(); v++)
				delete *v;
			break;
		case SELECT_MODE_GROUP:
		case SELECT_MODE_TRIANGLE:
			for(t=selected_t.begin(); t!=selected_t.end(); t++)
				delete *t;
			break;
	}
	ClearSelection();
}
