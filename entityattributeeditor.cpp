#include "allinc.h"

void EntityAttributeEditor::Init(CEntity *e, QWidget *parent)
{
	setupUi(this);

	this->parent = parent;

	connect(this, SIGNAL(accepted()), this, SLOT(Accepted()));
	connect(this, SIGNAL(rejected()), this, SLOT(Rejected()));

	connect(Vec3RadioButton, SIGNAL(toggled(bool)), this, SLOT(ChangeType()));
	connect(Vec2RadioButton, SIGNAL(toggled(bool)), this, SLOT(ChangeType()));
	connect(FloatRadioButton, SIGNAL(toggled(bool)), this, SLOT(ChangeType()));
	connect(IntRadioButton, SIGNAL(toggled(bool)), this, SLOT(ChangeType()));
	connect(StringRadioButton, SIGNAL(toggled(bool)), this, SLOT(ChangeType()));

	this->entity = e;

	parent->setEnabled(false);
	setEnabled(true);
}

EntityAttributeEditor::EntityAttributeEditor(CEntity *e, QWidget *parent) : QDialog(parent), Ui::EntityAttributeEditor()
{
	Init(e, parent);

	create = true;

	name = original_name = string("");
	type = CEntityAttribute::VECTOR;

	attribute = 0;

	CopyContentToUi();
	RefreshUi();
}


EntityAttributeEditor::EntityAttributeEditor(CEntity *e, string name, CEntityAttribute *a, QWidget *parent) : QDialog(parent), Ui::EntityAttributeEditor()
{
	Init(e, parent);

	create = false;

	this->attribute = a;
	this->name = original_name = name;
	type = attribute->type;
	switch(type)
	{
		case CEntityAttribute::VECTOR:		vec_v = attribute->vec_v;		break;
		case CEntityAttribute::VECTOR2:		vec2_v = attribute->vec2_v;		break;
		case CEntityAttribute::FLOAT:		float_v = attribute->float_v;	break;
		case CEntityAttribute::INT:			int_v = attribute->int_v;		break;
		case CEntityAttribute::STRING:		string_v = attribute->string_v;	break;
	}

	CopyContentToUi();
	RefreshUi();
}

EntityAttributeEditor::~EntityAttributeEditor()
{
	parent->setEnabled(true);
}


void EntityAttributeEditor::closeEvent(QCloseEvent *event)
{
	QDialog::closeEvent(event);
	parent->setEnabled(true);
}


void EntityAttributeEditor::Accepted(void)
{
	CopyContentFromUi();
	CopyContentToAttribute();
	close();
	parent->setEnabled(true);
}


void EntityAttributeEditor::Rejected(void)
{
	close();
	parent->setEnabled(true);
}

void EntityAttributeEditor::ChangeType(void)
{
	CEntityAttribute::Type new_type;

	CopyContentFromUi();

	if(Vec3RadioButton->isChecked())
		new_type = CEntityAttribute::VECTOR;
	else if(Vec2RadioButton->isChecked())
		new_type = CEntityAttribute::VECTOR2;
	else if(FloatRadioButton->isChecked())
		new_type = CEntityAttribute::FLOAT;
	else if(IntRadioButton->isChecked())
		new_type = CEntityAttribute::INT;
	else if(StringRadioButton->isChecked())
		new_type = CEntityAttribute::STRING;
	else
		new_type = CEntityAttribute::VECTOR;

	if(type == new_type)
		return;

	switch(new_type)
	{
		case CEntityAttribute::VECTOR: vec_v = Vec(0.0, 0.0, 0.0); break;
		case CEntityAttribute::VECTOR2: vec2_v = Vec(0.0, 0.0);	break;
		case CEntityAttribute::FLOAT: float_v = 0.0; break;
		case CEntityAttribute::INT: int_v = 0.0; break;
		case CEntityAttribute::STRING: string_v.clear(); break;
	}

	type = new_type;
	CopyContentToUi();
	RefreshUi();
}

void EntityAttributeEditor::RefreshUi(void)
{
	Vec3Widget->setVisible(type == CEntityAttribute::VECTOR);
	Vec2Widget->setVisible(type == CEntityAttribute::VECTOR2);
	FloatWidget->setVisible(type == CEntityAttribute::FLOAT);
	IntWidget->setVisible(type == CEntityAttribute::INT);
	StringWidget->setVisible(type == CEntityAttribute::STRING);
}

void EntityAttributeEditor::CopyContentFromUi(void)
{
	switch(type)
	{
		case CEntityAttribute::VECTOR:
			vec_v.x = Vec3XEdit->value();
			vec_v.y = Vec3YEdit->value();
			vec_v.z = Vec3ZEdit->value();
			break;
		case CEntityAttribute::VECTOR2:
			vec2_v.x = Vec2XEdit->value();
			vec2_v.y = Vec2YEdit->value();
			break;
		case CEntityAttribute::FLOAT: float_v = FloatEdit->value(); break;
		case CEntityAttribute::INT: int_v = IntEdit->value(); break;
		case CEntityAttribute::STRING: string_v = StringEdit->toPlainText().toStdString(); break;
	}

	name = NameEdit->text().toStdString();

}

void EntityAttributeEditor::CopyContentToUi(void)
{
	NameEdit->setText(QString::fromStdString(name));

	switch(type)
	{
		case CEntityAttribute::VECTOR:
			Vec3XEdit->setValue(vec_v.x);
			Vec3YEdit->setValue(vec_v.y);
			Vec3ZEdit->setValue(vec_v.z);
			Vec3RadioButton->setChecked(true);
			break;
		case CEntityAttribute::VECTOR2:
			Vec2XEdit->setValue(vec2_v.x);
			Vec2YEdit->setValue(vec2_v.y);
			Vec2RadioButton->setChecked(true);
			break;
		case CEntityAttribute::FLOAT: FloatEdit->setValue(float_v); FloatRadioButton->setChecked(true); break;
		case CEntityAttribute::INT: IntEdit->setValue(int_v); IntRadioButton->setChecked(true); break;
		case CEntityAttribute::STRING: StringEdit->setPlainText(QString::fromStdString(string_v)); StringRadioButton->setChecked(true); break;
	}

	NameEdit->setText(QString::fromStdString(name));
}

void EntityAttributeEditor::CopyContentToAttribute(void)
{
	if(!create)
		entity->RemoveAttribute(original_name);
	else
		attribute = new CEntityAttribute();

	entity->attributes.insert(pair<string, CEntityAttribute *>(name, attribute));
	attribute->type = type;

	switch(type)
	{
		case CEntityAttribute::VECTOR: attribute->vec_v = vec_v; break;
		case CEntityAttribute::VECTOR2: attribute->vec2_v = vec2_v; break;
		case CEntityAttribute::FLOAT: attribute->float_v = float_v; break;
		case CEntityAttribute::INT: attribute->int_v = int_v; break;
		case CEntityAttribute::STRING: attribute->string_v = string_v; break;
	}
}






















