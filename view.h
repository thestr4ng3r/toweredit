#ifndef VIEW_H
#define VIEW_H

#include <QtGui>

#define PREVIEW_VIEW_FRONT      0
#define PREVIEW_VIEW_BACK       1
#define PREVIEW_VIEW_TOP        2
#define PREVIEW_VIEW_BOTTOM     3
#define PREVIEW_VIEW_LEFT       4
#define PREVIEW_VIEW_RIGHT      5

CVector ViewMultiplicator(int view);
CVector2 TransformToView(CVector v, int view);
CVector TransformFromView(CVector2 v, int view);
CVector2 TransformToFocus(CVector2 v, CVector2 focus);
CVector2 TransformTo2DField(CVector2 v, float zoom_factor, CVector2 rect);
CVector2 TransformToPreview(CVector2 v, CVector2 focus, float zoom_factor, CVector2 rect);
CVector2 TransformFromPreview(CVector2 v, CVector2 focus, float zoom_factor,  CVector2 rect);

void SetViewComboBox(QComboBox *b, int preset = 0);

void RefreshMeshLists(CMesh *mesh, QListWidget *TriangleList, QListWidget *TextureList, int &tri_nr, int &tex_nr, CTriangle **&tri_list, CMaterial **&tex_list, QComboBox *TextureName = 0, CTriangle *selected_t = 0, CMaterial *selected_m = 0);

#endif // VIEW_H
