
#ifndef TMAPPREVIEW_H
#define TMAPPREVIEW_H

#include "allinc.h"

#include <QtGui>

class TMapPreview : public QGraphicsView
{
	Q_OBJECT;

public:
	TMapPreview(QComboBox *mode, CTriangle ***t, int tn, CVector2 ***c, CMaterial **m);
	~TMapPreview(void);

public slots:
	void RefreshPreview(void);

private:
	QComboBox *modes;
	QGraphicsScene *g_scene;
	QPainter *painter;
	QPixmap pixmap, material_pixmap;
	QGraphicsPixmapItem *item;

	CVector2 min, max;

	CVector2 ***coords;
	CTriangle ***triangles;
	int triangle_nr;
	CMaterial **material;
	CMaterial *loaded_material;

	CTriangle **selection;
	int selection_nr;

	CVector2 mark_point[2];

	float scale;
	CVector2 offset;

	int view;

	int mouse_right_pressed;
	int mouse_left_pressed;
	int old_mouse_x, old_mouse_y;

	CVector2 TransformToTex(CVector2 v) { return v - min; };
	CVector2 TransformToPix(CVector2 v) { return v + min; };

protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);
};

#endif // TMAPPREVIEW_H
