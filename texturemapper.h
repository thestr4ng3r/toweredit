
#ifndef TEXTUREMAPPER_H
#define TEXTUREMAPPER_H

#include "allinc.h"

#include <QtGui>
#include "ui_texturemapper.h"

class TextureMapper : public QDialog, private Ui::TextureMapper
{
	Q_OBJECT

	public:
		TextureMapper(CTriangle **t, int tn, CMaterial **m, int mn, QWidget *parent = 0);
		~TextureMapper(void);

	public slots:
		void PlanarMap(void);
		void CylindricalMap(void);
		void SetMaterial(void);

	private:
		QWidget *parent;

		TMapPreview *preview;

		CVector2 **coords;
		CTriangle **triangles;
		int triangle_nr;
		CMaterial **materials;
		int material_nr;

		CMaterial *selected_material;

		void RefreshMaterialList(void);

	protected:
		void closeEvent(QCloseEvent *event);
		void timerEvent(QTimerEvent *event);
};

#endif // TEXTUREMAPPER_H
