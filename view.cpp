#include "allinc.h"


//#define PREVIEW_VIEW_FRONT      0
//#define PREVIEW_VIEW_BACK       1
//#define PREVIEW_VIEW_TOP        2
//#define PREVIEW_VIEW_BOTTOM     3
//#define PREVIEW_VIEW_LEFT       4
//#define PREVIEW_VIEW_RIGHT      5

const float view_matrices[][3*2] = { // 3*2 matrices to convert 3d coordinates to 2d preview
	{  1.0,  0.0,  0.0,	// PREVIEW_VIEW_FRONT
	   0.0, -1.0,  0.0 },

	{ -1.0,  0.0,  0.0,	// PREVIEW_VIEW_BACK
	   0.0, -1.0,  0.0 },

	{  1.0,  0.0,  0.0,	// PREVIEW_VIEW_TOP
	   0.0,  0.0,  1.0 },

	{ -1.0,  0.0,  0.0,	// PREVIEW_VIEW_BOTTOM
	   0.0,  0.0,  1.0 },

	{  0.0,  0.0,  1.0,	// PREVIEW_VIEW_LEFT
	   0.0, -1.0,  0.0 },

	{  0.0,  0.0, -1.0,	// PREVIEW_VIEW_RIGHT
	   0.0, -1.0,  0.0 },
};

CVector2 TransformToFocus(CVector2 v, CVector2 focus)
{
    return v - focus;
}

CVector2 TransformTo2DField(CVector2 v, float zoom_factor, CVector2 rect)
{
    return ((rect * 0.5) + (v * zoom_factor));
}

CVector2 TransformToPreview(CVector2 v, CVector2 focus, float zoom_factor, CVector2 rect)
{
    return TransformTo2DField(TransformToFocus(v, focus), zoom_factor, rect);
}

CVector2 TransformFromPreview(CVector2 v, CVector2 focus, float zoom_factor, CVector2 rect)
{
    CVector2 p = v - (rect * 0.5);
    return focus + p * (1.0 / zoom_factor);
}

CVector ViewMultiplicator(int view)
{
    switch(view)
    {
    case PREVIEW_VIEW_BACK: case PREVIEW_VIEW_FRONT: return Vec(1.0, 1.0, 0.0);
    case PREVIEW_VIEW_BOTTOM: case PREVIEW_VIEW_TOP: return Vec(1.0, 0.0, 1.0);
    case PREVIEW_VIEW_RIGHT: case PREVIEW_VIEW_LEFT: return Vec(0.0, 1.0, 1.0);
    }

    return Vec(0.0, 0.0, 0.0);
}

CVector2 TransformToView(CVector v, int view)
{
    /*switch(view)
    {
    case PREVIEW_VIEW_FRONT: return Vec(v.x, -v.y);
    case PREVIEW_VIEW_BACK: return Vec(-v.x, -v.y);
    case PREVIEW_VIEW_TOP: return Vec(v.x, v.z);
    case PREVIEW_VIEW_BOTTOM: return Vec(-v.x, v.z);
    case PREVIEW_VIEW_LEFT: return Vec(v.z, -v.y);
    case PREVIEW_VIEW_RIGHT: return Vec(-v.z, -v.y);
    }

    return Vec(0.0, 0.0);*/

	return Vec(	view_matrices[view][0+0] * v.x + view_matrices[view][0+1] * v.y + view_matrices[view][0+2] * v.z,
			view_matrices[view][3+0] * v.x + view_matrices[view][3+1] * v.y + view_matrices[view][3+2] * v.z);
}

CVector TransformFromView(CVector2 v, int view)
{
    switch(view)
    {
    case PREVIEW_VIEW_FRONT: return Vec(v.x, -v.y, 0.0);
    case PREVIEW_VIEW_BACK: return Vec(-v.x, -v.y, 0.0);
    case PREVIEW_VIEW_TOP: return Vec(v.x, 0.0, v.y);
    case PREVIEW_VIEW_BOTTOM: return Vec(-v.x, 0.0, v.y);
    case PREVIEW_VIEW_LEFT: return Vec(0.0, -v.y, v.x);
    case PREVIEW_VIEW_RIGHT: return Vec(0.0, -v.y, -v.x);
    }

    return Vec(0.0, 0.0, 0.0);
}

void SetViewComboBox(QComboBox *b, int preset)
{
    int i;
    static const char *modes[] = { "Front (Z+)", "Back (Z-)", "Top (Y+)", "Bottom (Y-)", "Left (X-)", "Right (X+)" };
    b->clear();
    for(i=0; i<6; i++)
        b->addItem(modes[i]);

    b->setCurrentIndex(preset);
}

void RefreshMeshLists(CMesh *mesh, QListWidget *TriangleList, QListWidget *TextureList, int &tri_nr, int &tex_nr, CTriangle **&tri_list, CMaterial **&tex_list, QComboBox *TextureName, CTriangle *selected_t, CMaterial *selected_m)
{
    CTriangle *t;
    CMaterial *tx;
    char *temp = 0;
    char *selected = 0;
    int i;

    temp = (char *)malloc(200);

    if(!mesh)
        return;

    if(!mesh->GetState())
    {
        if(TextureName)
            TextureName->addItem("$NONE");
        return;
    }

    if(TriangleList)
    {
        tri_nr = mesh->GetTriangleCount();
        tex_nr = mesh->GetMaterialCount();

        selected = new char[2];
        tri_list = new CTriangle *[tri_nr];
        TriangleList->clear();

        for(i=0; i<mesh->GetTriangleCount(); i++)
        {
        	t = mesh->GetTriangle(i);
            if(selected_t == t)
                strcpy(selected, "> ");
            else
                strcpy(selected, "");

            tri_list[i] = t;
            if(t->MaterialState())
            {
                snprintf(temp, 200, "%s%s; %f, %f, %f; %f, %f, %f; %f, %f, %f", selected, t->mat->name, t->v[0]->x, t->v[0]->y, t->v[0]->z,
                    t->v[1]->x, t->v[1]->y, t->v[1]->z, t->v[2]->x, t->v[2]->y, t->v[2]->z);
            }
            else
            {
                snprintf(temp, 200, "%s$NONE; %f, %f, %f; %f, %f, %f; %f, %f, %f", selected, t->v[0]->x, t->v[0]->y, t->v[0]->z,
                        t->v[1]->x, t->v[1]->y, t->v[1]->z, t->v[2]->x, t->v[2]->y, t->v[2]->z);
            }


            TriangleList->addItem(QString::fromLocal8Bit(temp, strlen(temp)));
        }
    }

    if(TextureList)
    {
        tex_list = new CMaterial *[tex_nr];
        TextureList->clear();
        if(TextureName)
        {
            TextureName->clear();
            TextureName->addItem("$NONE");
        }
        for(i=0; i<mesh->GetMaterialCount(); i++)
        {
        	tx = mesh->GetMaterial(i);
            if(selected_m == tx)
                strcpy(selected, "> ");
            else
                strcpy(selected, "");

            tex_list[i] = tx;
            snprintf(temp, 200, "%s%s", selected, tx->name);

            TextureList->addItem(QString(temp));
            if(TextureName)
                TextureName->addItem(QString(temp));
        }
    }
}
