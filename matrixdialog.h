
#ifndef MATRIXDIALOG_H
#define MATRIXDIALOG_H

#include "allinc.h"

#include <QtGui>
#include "ui_matrixdialog.h"

class MatrixDialog : public QDialog, private Ui::MatrixDialog
{
	Q_OBJECT

	public:
		MatrixDialog(Selection *s, QWidget *parent = 0);
		~MatrixDialog();

	public slots:
		void Click(QAbstractButton *b);

	private:
		QWidget *parent;

		void Cancel(void);
		void Ok(void);

		Selection *selection;

	protected:
		void closeEvent(QCloseEvent *event);
};

#endif // TEXTUREMAPPER_H
