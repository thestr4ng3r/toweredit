#include "allinc.h"

MatrixDialog::MatrixDialog(Selection *s, QWidget *parent) : QDialog(parent)
{
	setupUi(this);
	
	this->parent = parent;
	
	selection = s;	
	s->SwitchToVertexSelectMode();
	
	connect(Buttons, SIGNAL(clicked(QAbstractButton *)), this, SLOT(Click(QAbstractButton *)));
	
	parent->setEnabled(false);
	setEnabled(true);
}

MatrixDialog::~MatrixDialog()
{
    parent->setEnabled(true);
}

void MatrixDialog::Click(QAbstractButton *b)
{
	switch(Buttons->standardButton(b))
	{
		case QDialogButtonBox::Ok:
			Ok();
			break;
		case QDialogButtonBox::Cancel:
			Cancel();
			break;
		default:
			break;
	}
}

void MatrixDialog::closeEvent(QCloseEvent *event)
{
    QDialog::closeEvent(event);

    parent->setEnabled(true);
}

void MatrixDialog::Ok(void)
{
	CVertex *v;
	CVector a;
	set<CVertex *>::iterator it;

	float mat[16] = {
		v00->value(), v10->value(), v20->value(), v30->value(),
		v01->value(), v11->value(), v21->value(), v31->value(),
		v02->value(), v12->value(), v22->value(), v32->value(),
		0.0,          0.0,          0.0,          1.0 };


	for(it=selection->selected_v.begin(); it!=selection->selected_v.end(); it++)
	{
		v = *it;
		a = ApplyMatrix4(mat, *v);
		v->x = a.x; v->y = a.y; v->z = a.z;
	}

	close();
}

void MatrixDialog::Cancel(void)
{
	close();
}

