QT += opengl
TARGET = toweredit
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    glwin.cpp \
    preview2d.cpp \
    tmappreview.cpp \
    texturemapper.cpp \
    matrixdialog.cpp \
    image.cpp \
    view.cpp \
    selection.cpp \
    editor.cpp \
    objloader.cpp \
    normalscalculator.cpp \
    entityattributeeditor.cpp
HEADERS += mainwindow.h \
    glwin.h \
    allinc.h \
    preview2d.h \
    tmappreview.h \
    texturemapper.h \
    matrixdialog.h \
    defines.h \
    image.h \
    view.h \
    selection.h \
    editor.h \
    objloader.h \
    normalscalculator.h \
    entityattributeeditor.h
INCLUDEPATH += /usr/include/towerengine \
    /usr/include/glib-2.0 \
    /usr/lib/glib-2.0/include \
    /usr/include/glib-2.0 \
    /usr/include/libxml2 \
    /usr/lib/i386-linux-gnu/glib-2.0/include
FORMS += mainwindow.ui \
    texturemapper.ui \
    matrixdialog.ui \
    normalscalculator.ui \
    entityattributeeditor.ui
LIBS += -ltowerengine \
    -lGL -lGLU \
    -l3ds \
    -lg3d \
    -lobj \
    -lglib-2.0 \
    -lxml2 \
    -lIL \
    -lILU
QMAKE_CXXFLAGS += `pkg-config --cflags glib-2.0`
QMAKE_LFLAGS += `pkg-config --libs glib-2.0`
DEFINES += TMS_USE_LIB_G3D \
	TMS_USE_LIB_3DS
RESOURCES += resource.qrc
CONFIG += debug
