#include "allinc.h"

TextureMapper::TextureMapper(CTriangle **t, int tn, CMaterial **m, int mn, QWidget *parent) : QDialog(parent)
{
	int i;
	
	setupUi(this);
	
	this->parent = parent;
	
	triangles = t;
	triangle_nr = tn;
	materials = m;
	material_nr = mn;
	
	selected_material = materials[0];
	
	connect(PlanarMapButton, SIGNAL(clicked()), this, SLOT(PlanarMap()));
	connect(CylindricalMapButton, SIGNAL(clicked()), this, SLOT(CylindricalMap()));
	connect(OkButton, SIGNAL(clicked()), this, SLOT(close()));
	connect(MaterialList, SIGNAL(itemSelectionChanged()), this, SLOT(SetMaterial()));
	
	coords = new CVector2 *[triangle_nr];
	*coords = new CVector2[3];
	
	SetViewComboBox(PlanarView);
	
	preview = new TMapPreview(PlanarView, &triangles, triangle_nr, &coords, &selected_material);
	PreviewLayout->addWidget(preview);
	preview->show();
	
	parent->setEnabled(false);
	setEnabled(true);
	
	RefreshMaterialList();
	
	for(i=0; i<material_nr; i++)
		if(strcmp(materials[i]->name, triangles[0]->m_name) == 0)
	    {
	    	selected_material = materials[i];
	            MaterialList->setCurrentRow(i);;
	    	break;
	    }
	
	startTimer(11);
}

TextureMapper::~TextureMapper(void)
{
    parent->setEnabled(true);
}

void TextureMapper::timerEvent(QTimerEvent *event)
{
    QDialog::timerEvent(event);

    preview->RefreshPreview();
}

void TextureMapper::closeEvent(QCloseEvent *event)
{
    QDialog::closeEvent(event);

    parent->setEnabled(true);
}

void TextureMapper::SetMaterial(void)
{
    selected_material = materials[MaterialList->currentRow()];
}

void TextureMapper::PlanarMap(void)
{
    int view = PlanarView->currentIndex();
    CVector2 coords[triangle_nr][3];
    int i, v;
    CVector2 max_real, min_real;
    CVector2 min, max;
    float temp;

    min.Set(MinXPlanarMap->value(), MinYPlanarMap->value());
    max.Set(MaxXPlanarMap->value(), MaxYPlanarMap->value());

    if(PlanarXMirrorCheckBox->checkState())
    {
        temp = min.x;
        min.x = max.x;
        max.x = temp;
    }

    if(PlanarYMirrorCheckBox->checkState())
    {
        temp = min.y;
        min.y = max.y;
        max.y = temp;
    }

    max_real.Set(-INFINITY, -INFINITY); min_real.Set(INFINITY, INFINITY);

    for(i=0; i<triangle_nr; i++)
        for(v=0; v<3; v++)
        {
            coords[i][v] = TransformToView(*triangles[i]->v[v], view);
            if(coords[i][v].x > max_real.x)
                max_real.x = coords[i][v].x;
            if(coords[i][v].y > max_real.y)
                max_real.y = coords[i][v].y;
            if(coords[i][v].x < min_real.x)
                min_real.x = coords[i][v].x;
            if(coords[i][v].y < min_real.y)
                min_real.y = coords[i][v].y;
        }

    for(i=0; i<triangle_nr; i++)
        for(v=0; v<3; v++)
        {
            coords[i][v] -= min_real;
            coords[i][v] /= max_real - min_real;
            coords[i][v] *= max - min;
            coords[i][v] += min;
            triangles[i]->mat = selected_material;
	    triangles[i]->v[v]->uv.Set(coords[i][v].x, coords[i][v].y);
        }
}

void TextureMapper::CylindricalMap(void)
{
	int i, v;
	float temp;
	float min_h, max_h;
	float h;
	float angle[3];
	CVector center, vec;
	CVector vx, vz;
	float x, y;
	CVector sh;
	CVector2 min, max;
	CVector2 coord[3];

	vec = Vec(VecXCylindricalMap->value(), VecYCylindricalMap->value(), VecZCylindricalMap->value());
	vec.Normalize();
	if(Dot(vec, Vec(0.0, 1.0, 0.0)) > 0.95)
	{
		vx = Vec(1.0, 0.0, 0.0);
		vz = Vec(0.0, 0.0, 1.0);
	}
	else
	{
		vx = Cross(vec, Vec(0.0, 1.0, 0.0));
		vx.Normalize();
		vz = Cross(vec, vx);
		vz.Normalize();
	}
	center = Vec(CenterXCylindricalMap->value(), CenterYCylindricalMap->value(), CenterZCylindricalMap->value());
	min.Set(MinXCylindricalMap->value(), MinYCylindricalMap->value());
	max.Set(MaxXCylindricalMap->value(), MaxYCylindricalMap->value());
	
	if(PlanarXMirrorCheckBox->checkState())
	{
	    temp = min.x;
	    min.x = max.x;
	    max.x = temp;
	}
	
	if(PlanarYMirrorCheckBox->checkState())
	{
	    temp = min.y;
	    min.y = max.y;
	    max.y = temp;
	}

	min_h = INFINITY;
	max_h = -INFINITY;

	for(i=0; i<triangle_nr; i++)
	{
		for(v=0; v<3; v++)
		{
	        	sh = *triangles[i]->v[v] - center; 
			h = Dot(sh, vec);
			if(h < min_h)
				min_h = h;
			if(h > max_h)
				max_h = h;
		}
	}

	for(i=0; i<triangle_nr; i++)
	{
		for(v=0; v<3; v++)
		{
			sh = *triangles[i]->v[v] - center; 
			h = Dot(sh, vec);		
			
			coord[v].y = (h - min_h) / (max_h - min_h);

			sh -= vec * h;
			sh.Normalize();
			x = Dot(sh, vx);
			y = Dot(sh, vz);
			angle[v] = atan2(y, x) + M_PI; //asin(y) + (M_PI / 2);
			//if(x < 0.0)
			//	angle[v] = 2*M_PI - angle[v];
		}

		if(angle[0] > M_PI * 1.5 && angle[1] < M_PI * 0.5 && angle[0] < M_PI * 2)
			angle[1] += 2*M_PI;
		if(angle[0] > M_PI * 1.5 && angle[2] < M_PI * 0.5 && angle[0] < M_PI * 2)
			angle[2] += 2*M_PI;
		if(angle[1] > M_PI * 1.5 && angle[0] < M_PI * 0.5 && angle[1] < M_PI * 2)
			angle[0] += 2*M_PI;
		if(angle[1] > M_PI * 1.5 && angle[2] < M_PI * 0.5 && angle[1] < M_PI * 2)
			angle[2] += 2*M_PI;
		if(angle[2] > M_PI * 1.5 && angle[0] < M_PI * 0.5 && angle[2] < M_PI * 2)
			angle[0] += 2*M_PI;
		if(angle[2] > M_PI * 1.5 && angle[1] < M_PI * 0.5 && angle[2] < M_PI * 2)
			angle[1] += 2*M_PI;

		for(v=0; v<3; v++)
		{
			coord[v].x = angle[v] / (2*M_PI);
			coord[v] += min;
			coord[v] *= (max - min);
			triangles[i]->v[v]->uv.Set(coord[v].x, coord[v].y);
			triangles[i]->mat = selected_material;
		}
	}
}

void TextureMapper::RefreshMaterialList(void)
{
    int i;

    MaterialList->clear();

    for(i=0; i<material_nr; i++)
        MaterialList->addItem(QString::fromLocal8Bit(materials[i]->name));
}













