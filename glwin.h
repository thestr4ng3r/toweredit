
#ifndef _GLWIN_H
#define _GLWIN_H

#include "allinc.h"

#include <QGLWidget>

class GLWin : public QGLWidget
{
	Q_OBJECT

 public:
	 GLWin(Editor *editor, QWidget *parent = 0);
	 ~GLWin();

	 void SetSelection(Selection *selection) { public_selection = selection; }
	 void PaintGL();
	 void InitializeGL();
	 void ResetFocus(void) { focus = Vec(0.0, 0.0, 0.0); }
	 void CalculateCamPos(void);

	 CTriangle *preview_t;

	 int preview;

private:
	void ResizeGL(int width, int height);
	void PaintAxes(void);
	void PaintNormals(void);
	CVector CalculateCamTo(void) { return focus - cam_pos; }

	Editor *editor;

	GLuint x_tex, y_tex, z_tex;
	int width, height;
	float view_angle;

	float cam_dist;
	CVector focus;
	CVector focus_move_x, focus_move_y;
	CVector2 cam_rot;
	CVector cam_pos;

	Selection *public_selection;

	int mouse_pressed;
	int old_mouse_x, old_mouse_y;

 protected:
	 void paintGL() { PaintGL(); }
	 void initializeGL() { InitializeGL(); }
	 void resizeEvent(QResizeEvent *event);
	 void mousePressEvent(QMouseEvent *event);
	 void mouseReleaseEvent(QMouseEvent *event);
	 void mouseMoveEvent(QMouseEvent *event);
	 void wheelEvent(QWheelEvent *event);
};

#endif // GLWIN_H
