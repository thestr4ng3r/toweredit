
#ifndef PREVIEW2D_H
#define PREVIEW2D_H

#include "allinc.h"

#include <QtCore>
#include <QtGui>

class MainWindow;

class Preview2D : public QGraphicsView
{
    Q_OBJECT

public:
    Preview2D();
    Preview2D(QComboBox *c, Selection *selection, MainWindow *main_win);
    ~Preview2D(void);
    void Create(QComboBox *c, Selection *selection, MainWindow *main_win);
    void SetMesh(CMesh *m);

    int redraw;
    int selection_changed;
public slots:
    void RefreshView(int index = -1);
    void RefreshPreview(void);

    void SetZoomFactor(float zoom) { zoom_factor = zoom; };

private:
    MainWindow *main_win;
    QComboBox *modes;
    QGraphicsScene *g_scene;
    QPainter *painter;
    QPixmap pixmap;
    QGraphicsPixmapItem *item;

    CVector2 focus;

    CMesh *mesh;
    CTriangle ***public_selected_t;
    CVertex ***public_selected_v;
    Selection *public_selection;
    int *edit_mode;
    CVector2 select_point[2];
    CVertex **transform_v;
    int transform_v_nr;
    CVector scale_center;
    CVector2 scale_center_2d;
    CVector2 scale_origin;
    float scale_base, scale_factor;

    int view;

    int mouse_right_pressed;
    int mouse_left_pressed;
    int edit_action;
    int old_mouse_x, old_mouse_y;

    float zoom_factor;

    CVector2 Transform(CVector2 v);
    CVector2 Transform(CVector v) { return Transform(TransformToView(v, view)); };
    CVector2 ReTransform(CVector2 v);
    CVector ReTransformVRel(CVector2 v) { return TransformFromView(v * (1 / zoom_factor), view); };

    void SelectVertices(float x1, float y1, float x2, float y2, int add = 0);
    void SelectTriangles(float x1, float y1, float x2, float y2, int add = 0);
    void SelectGroups(float x1, float y1, float x2, float y2, int add = 0);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void resizeEvent(QResizeEvent *event);
};

#endif // PREVIEW2D_H
