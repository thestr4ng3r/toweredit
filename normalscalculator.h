#ifndef _NORMALSCALCULATOR_H
#define _NORMALSCALCULATOR_H

#include "allinc.h"

#include <QDialog>
#include "ui_normalscalculator.h"

class NormalsCalculator : public QDialog, Ui::NormalsCalculator
{
	Q_OBJECT

public:
	NormalsCalculator(Editor *editor, QWidget *parent = 0);
	~NormalsCalculator();

public slots:
	void Click(QAbstractButton *b);

private:
	QWidget *parent;
	Editor *editor;

	void Cancel(void);
	void Ok(void);

protected:
	void showEvent(QShowEvent *event);
	void closeEvent(QCloseEvent *event);
};

#endif // _NORMALSCALCULATOR_H
